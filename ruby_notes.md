# Ruby Notes
I wanna learn ruby and community seems like the least toxic place out of all the other language communities I've seen... so here we are.

# Before we start... 
Ruby is an interpreted, high-level, general-purpose programming language. It was designed and developed in the mid-1990s by Yukihiro "Matz" Matsumoto in Japan.

Be nice. Be like Matz. 

## Intro 

### Data Types: Numbers, Strings, Booleans
Ruby is _dynamically typed_. So datatypes are interpreted at runtime. 
```
# Some basic datatypes
my_num = 25
my_boolean = true
my_string = "Ruby";
```
Semi-colons are *not* required in Ruby, but they can be used!

### Math
There are six built-in math operators that we can use in Ruby. 
```
Addition (+)

Subtraction (-)

Multiplication (*)

Division (/)

Exponentiation (**)

Modulo (%)
```
### Puts? Why not print?
`print` takes whatever is passed to it and passes it to the screen. 

`puts` for ("_put string_") adds a new (blank) line after the thing you want it to print.
```
print "Hello "
puts "World!"

# Returns Hello World!
```

### Everything in Ruby is an Object
Since everything in Ruby is an object, everything also has some built-in functions as well (*methods*). 

You can think of methods as “skills” that certain objects have. For instance, strings (words or phrases) have built-in methods that can tell you the length of the string, reverse the string, and more.

An example of this is the `.length` method. 
```
print "Lake".length

# ==> 4
```
There is also a `.reverse` method as well.
```
print "Lake".reverse

# ==> ekaL
```
Changing cases too? 
```
puts "Lake".upcase
puts "Lake".downcase

# ==> LAKE
# ==> lake 
```

### Multi-line comments
Multi-line comments in this language are wild af. 

Use `=begin` to start a multi-line comment and use `=end` to end the comment
```
=begin
What the... this is crazy.

Everyhing in here is comment!
=end
```

### Call chaining
We can chain method calls together if the return type is compatible with the next method call!
```
name = "Lake"

puts name.downcase.reverse.upcase

# ==> EKAL
```

## Basics
The basics baby!

### Promting the user
In order to get our program to get any input from the user, we use the `gets` method. Which _gets_ input from the user. 

When getting input, Ruby automatically adds a blank line (or *newline*) after each bit of input. To avoid this we can use `chomp`. 

`chomp` removes that extra line.
```
print "What's your first name? "

first_name = gets.chomp 

print "What's your last name? "

last_name = gets.chomp

print "What city are you from? "

city = gets.chomp

print "What state are you from? "

state = gets.chomp
state = state.upcase

puts "#{first_name}"
puts "#{last_name}"
puts "#{city}, #{state}"
```

*NOTE:* The `#{some_variable}` is how ruby does string interpolation.

### !
This is some cool syntactic sugar if you ask me. Lets look at this little line of code real quick.
```
first_name.capitalize!
```
What is the `!` doing? 

This modifies the value contained within the variable `first_name` itself.

## Control Flow
Control that program flow baby 😎

### If
`if` statements in Ruby require the `end` key word!
Simple example.
```
if 1 < 2
    print "YE YE!"
end 
```

### Else
Simple example
```
if 1 < 2
    print "YE YE!"
else 
    print "The universe is breaking as we speak..."
end 
```

### Elsif
This is just an else BUT it accepts a true/false condition!
```
if 1 < 2
    print "YE YE!"
elsif 4 < 8
    print "YAH YAH"
else 
    print "The universe is breaking as we speak..."
end 
```

### Unless
Unless is kind of a cool thing. 

If we want to check if something is false, we could just flip an `if`/`else`. 

OR we could use an `unless` statement!

For example,, let's say you don't want to eat _unless_ you're hungry. That is, while you're not hungry, write programs, but if you _are_ hungry, eat something! The program might look something like this:
```
hungry = false

unless hungry
  puts "I'm writing Ruby programs!"
else
  puts "Time to eat!"
end

# another example
issue = false 

print "We goo to go!" unless issue
```

### Logical
Ruby has the standard logical operators!

* `==` (equal to)
* `!=` (not equal to)
* `<` (less than)
* `>` (greater than)
* `<=` (less than or equal to)
* `>=` (greater than or equal to)
* `&&` (and)
* `||` (or)

### include?
Ruby has a built-in method called `include?` which will evaluate to `true` if it finds what it's looking for and `false` otherwise.

*NOTE*: As a general rule, Ruby methods that end with a `?` evaluate to boolean `true` or `false`!
```
print "Please input something here: "
user_input = gets.chomp
user_input.downcase!

if user_input.include? "s"
  print "s found"
end
```

### gsub!
Looking at the above code example, what if we want to replace all the occurnces of _s_ in a string?

We can do this with Ruby's `.gsub!` method! (*Global substitution*)
```
print "Please input something here: "
user_input = gets.chomp
user_input.downcase!

if user_input.include? "s"
  puts "s found"
  user_input.gsub!(/s/,"th")
  puts "#{user_input}"
end
```

## Loops and Iterators
Now we can look at the loops and iterators!

### While Loop
```
i = 0
while i < 5
  puts i
  i += 1
end
```

### Until Loop
The `until` loop is like the _compliment_ to the `while` loop!

It instructs the program to run the loop UNTIL a condition is met.
```
counter = 1
until counter > 10
  puts counter
  counter += 1
end
```

### For Loop
`for` loop syntax is probably much more compact than what you've seen before.

For loop range syntax is as follows:
```
# inclusive (will print out to 21)
for num in 1..15
  puts num
end
# # => 1 ->2 ->3 ->4 ->5 ->6 ->7 ->8 ->9 ->10 ->11 ->12 ->13 ->14 ->15 ->16 ->17 ->18 ->19 ->20 ->21 ->

# exclusive (will print out to 20)
for i in 1...21
  print "#{i} ->"
end
# => 1 ->2 ->3 ->4 ->5 ->6 ->7 ->8 ->9 ->10 ->11 ->12 ->13 ->14 ->15 ->16 ->17 ->18 ->19 ->20 ->
```

### Loop method
An iterator is just a Ruby method that repeatedly invokes a block of code. The code block is just the bit that contains the instructions to be repeated, and those instructions can be just about anything you like!

The simplest iterator is the loop method! We need a break` condition so the loop isn't infinite!
```
i = 20
loop do
  i -= 1
  print "#{i} "
  break if i <= 0
end

# => 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0 
```

### Next!
The `next` keyword can be used to skip over certain steps in a loop

It will allow us to continue the loop after we skip a certain step in the loop!
```
i = 20
loop do
  i -= 1
  next if i % 2 != 0 
  print "#{i} "
  break if i <= 0
end
```

### Arrays
For arrays, Ruby uses brackets to instantiate arrays!
```
my_array = []

for num in 0..4
  my_array[num] = num+1
end

print "#{my_array}"

# => [1, 2, 3, 4, 5] 
```

### .each
The `.each` method is much more powerful and useful compared to the `loop`/`do` method.

`.each` will apply an expression to each element of an object, one at a time.
```
odds = [1,3,5,7,9]

odds.each { |item| 
  placeholder = item * 2
  print "#{placeholder} "
}

# => 2 6 10 14 18 
```

### .times
The `.times` method is like a super compact for loop: it can perform a task on each item in an object a specified number of times.
```
3.times { puts "Three times baby" }
```

### .split
Ruby has a `.split` method that takes in a string and returns an array. 

We can _delimit_ the string we want to split by passing in a string to the method.
```
print "Input please: "
text = "These are some words to split"
words = text.split(" ")
print "#{words}"

# => ["these", "are", "some", "words", "to", "split"]
```

## Data Structures

### Arrays
To declare an array: `my_array = [1 ,2, 3]`

Arrays in Ruby are _zero indexed_!
```
# Print third element in array
demo_array = [100, 200, 300, 400, 500]

print "#{demo_array[2]}"
```

We can have multidimensional arrays as well!
```
multi_d_array = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]

multi_d_array.each { |x| puts "#{x}\n" }
```

### Hashes
A hash in Ruby is not like the encryption function. Instead it is more similar to an object in JS or a dictionary in Python! It is simply a collection of _key-value pairs_.

Values are assigned to keys with the `=>` operator.
```
my_hash = { 
  "name" => "Eric",
  "age" => 26,
  "hungry?" => true
}

puts my_hash["name"]
puts my_hash["age"]
puts my_hash["hungry?"]
```

The above is the _literal notation_ for creating a new hash. But what if we want to create an empty hash? We can use empty brackets (`{}`) or the `.new` method!
```
my_hash = Hash.new
```

If we want to add a key-value pair to an empty hash then we do the following:
```
pets = Hash.new

# hash[key] = value
pets["Davey"] = "Dog"

puts pets["Davey"]
```

### Iterating over arrays
It looks most optimal to use `.each` to iterate through an array.
```
languages = ["HTML", "CSS", "JavaScript", "Python", "Ruby"]

languages.each { |lang|
  puts lang
}
```

How do we use `.each` on multidimensional arrays?
```
s = [["ham", "swiss"], ["turkey", "cheddar"], ["roast beef", "gruyere"]]

s.each { |sub_array|
  sub_array.each { |item|
    puts item
  }
}
```

### Iterating over hashes
When iterating over hashes, we need two placeholder variables to represent each key/value pair.
```
secret_identities = {
  "The Batman" => "Bruce Wayne",
  "Superman" => "Clark Kent",
  "Wonder Woman" => "Diana Prince",
  "Freakazoid" => "Dexter Douglas"
}
  
secret_identities.each {|secret_id, name| 
  puts "#{secret_id}: #{name}"
}
```

## Practice!
Can you guess what the following program does?

Take a look! Copy and paste it into an editor and run it to see how it behaves!
```
puts "input please"
text = gets.chomp

words = text.split(" ")

frequencies = Hash.new(0)

words.each { |word|
  if frequencies.key?(word)
    frequencies[word] += 1
  else
    frequencies[word] = 1
  end
}

frequencies = frequencies.sort_by { |word, count|
  count
}
frequencies.reverse!

frequencies.each {|word, count| 
  puts "#{word} #{count}"
}
```

## Methods, Blocks, and Sorting

### Method Syntax
Methods are defined using the keyword `def`. 

Like other languages, methods generally have three parts:
1. _Header_: this includes `def`, the method name, and any parameters the method accepts
2. _Body_: Describes procedures that will execute when the method is called
3. _End_: We need to define the end of a method using the `end` keyword
```
# Methods dont always need parameters!
def puts_1_to_10
  (1..10).each { |i| puts i }
end

puts_1_to_10

# Method with parameters
def cubertino(n)
  puts n ** 3
end

cubertino(8)
```

### Splat
Speaking of not knowing what to expect: your methods not only don’t know what arguments they’re going to get ahead of time but occasionally, they don’t even know how many arguments there will be.

_How do we handle this?_

_Splat Arguments_: Splat arguments are arguments preceded by a `*`, which tells the program that the method can receive one or more arguments.
```
def what_up(greeting, *friends)
  friends.each { |friend| puts "#{greeting}, #{friend}!" }
end

what_up("What up", "Ian", "Zoe", "Zenas", "Eleanor")
```

### Returns
The `return` keyword tells Ruby to hand something back after some execution of code!
```
# You would never do this normally, but this is just to demonstrate how return works!

def add(n1, n2)
  return n1 + n2
end

num1 = 2
num2 = 3

res = add(num1, num2)

puts res

# Some more examples
def greeter(name)
  return "hello #{name}"
end

def by_three?(number)
  return number % 3 == 0
end
```

### Blocks are like nameless methods
Most methods that you’ve worked with have defined names that either you or someone else gave them (i.e. [array].sort(), “string”.downcase(), and so on). You can think of blocks as a way of creating methods that don’t have a name. (These are similar to anonymous functions in JavaScript or lambdas in Python.)

Blocks can be defined with either the keywords do and end or with curly braces ``(`{}`).

A method can take a block as a parameter. 

Passing a block to a method is a great way of _abstracting_ certain tasks from the method and defining those tasks when we call the method. Abstraction is an important idea in computer science, and you can think of it as meaning “making something simpler.”

### Combined comparison operator
We can also use a new operator called the combined comparison operator to compare two Ruby objects. The combined comparison operator looks like this: `<=>`. It returns `0` if the first operand (item to be compared) equals the second, `1` if the first operand is greater than the second, and `-1` if the first operand is less than the second.
```
books = ["Charlie and the Chocolate Factory", "War and Peace", "Utopia", "A Brief History of Time", "A Wrinkle in Time"]

# To sort our books in ascending order, in-place
books.sort! { |firstBook, secondBook| firstBook <=> secondBook }

# Sort your books in descending order, in-place below
books.sort! { |firstBook, secondBook| 
  secondBook <=> firstBook
}
```

## Hashes and Symbols
A little example to refresh your brain on Hashes!
```
matz = { "First name" => "Yukihiro",
  "Last name" => "Matsumoto",
  "Age" => 47,
  "Nationality" => "Japanese",
  "Nickname" => "Matz"
}

matz.each { |key, val|
  puts val
}
```

### Default values for Hashes
If we try to access a key that doesn't exist in a Hash we are working with, then Ruby will return `nil`. 

We can avoid this by creating a _default value_ when we instantiate the hash.
```
no_nil_hash = Hash.new("Not Nil")
```

### Symbols
We can certainly use strings as Ruby hash keys; as we’ve seen, there’s always more than one way to do something in Ruby. However, the Rubyist’s approach would be to use *symbols*.

You can think of a Ruby symbol as a sort of name. It’s important to remember that symbols aren’t strings:
```
"string" == :string 
# => false
```

Above and beyond the different syntax, there’s a key behavior of symbols that makes them different from strings. While there can be multiple different strings that all have the same value, there’s only one copy of any particular symbol at a given time.
```
puts "string".object_id
puts "string".object_id

puts :symbol.object_id
puts :symbol.object_id
```

*NOTE*: The `.object_id` method gets the ID of an object—it’s how Ruby knows whether two objects are the exact same object.

### Symbol Syntax
Symbols always start with a colon (`:`). They must be valid Ruby variable names, so the first character after the colon has to be a letter or underscore (`_`); after that, any combination of letters, numbers, and underscores is allowed.

### What are Symbols used for? 
Symbols pop up in a lot of places in Ruby, but they’re primarily used either as hash keys or for referencing method names.
```
symbol_hash = {
  :one => 1,
  :two => "two",
  :three => 3,
}
```

### Converting between symbols and strings
The `.to_s` and `.to_sym` methods are what you’re looking for!
```
strings = ["HTML", "CSS", "JavaScript", "Python", "Ruby"]

symbols = []
strings.each { |s|
  symbols.push(s.to_sym)
}

print symbols
```

### Symbols are faster than strings (for keys)
```
require 'benchmark'

string_AZ = Hash[("a".."z").to_a.zip((1..26).to_a)]
symbol_AZ = Hash[(:a..:z).to_a.zip((1..26).to_a)]

string_time = Benchmark.realtime do
  100_000.times { string_AZ["r"] }
end

symbol_time = Benchmark.realtime do
  100_000.times { symbol_AZ[:r] }
end

puts "String time: #{string_time} seconds."
puts "Symbol time: #{symbol_time} seconds."
```

### Being selective
We can use `.select` to pull specific values from a hash!
```
movie_ratings = {
  memento: 3,
  primer: 3.5,
  the_matrix: 5,
  truman_show: 4,
  red_dawn: 1.5,
  skyfall: 4,
  alex_cross: 2,
  uhf: 1,
  lion_king: 3.5
}

good_movies = movie_ratings.select { |movie, score|
  score > 3
}
```

### More Methods, More Solutions
We’ve often found we only want the key or value associated with a key/value pair, and it’s kind of a pain to put both into our block and only work with one. 

_Can we iterate over just keys or just values?_

This is Ruby. Of course we can.

Ruby includes two hash methods, `.each_key` and `.each_value`, that do exactly what you’d expect!
```
movie_ratings = {
  memento: 3,
  primer: 3.5,
  the_matrix: 3,
  truman_show: 4,
  red_dawn: 1.5,
  skyfall: 4,
  alex_cross: 2,
  uhf: 1,
  lion_king: 3.5
}

movie_ratings.each_key { |k|
  puts k  
}

movie_ratings.each_value { |v|
  puts v
}
```