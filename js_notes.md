# JavaScript Notes

Okay so this is starting at the functions section of CodeCademy so ill give a super condensed version of what came before this.

# Before we get started...

Below are some useful links!

* https://medium.com/@theflyingmantis/javascript-a-prototype-based-language-7e814cc7ae0b
* https://developer.mozilla.org/en-US/docs/Web/JavaScript/EventLoop
* https://medium.com/javascript-testing/here-a-secret-to-hack-and-launch-your-own-about-me-portfolio-site-4f5eff04f978
* https://scrimba.com/learn/bulma
* https://devcenter.heroku.com/articles/mean-apps-restful-api
* TBA...

## Basic output
Console logging is easy! You can pass string literals and variables using string interpolation using single quotes ('') and back-ticks (``) respectively!
```
console.log('This is a string');

var str = 'lime time!';
console.log(`It\'s ${str}`);
```

## Variable Declaration
There are three ways to declare a variable in js:
* var: a globally scoped variable or function
* let: a block scoped varibale or function
* const: a block scoped and immutable variable or function (TODO: fact check)

Variables are automatically type cast by the interpreter, so unless you want to change them later in your code you dont need to use `int`, `char`, and whatever. The following is an example of how to cast variables.

```
typeof 'John'; //returns string
typeof 2; //returns number
typeof true; //returns boolean
typeof [21, 6] //returns object

var x = 2;
x.toString(); //returns '2'

var num = 31;
String(num); //returns '31'

There are probs a shit load of other ways but, you get the idea? Good
```

## Conditionals
This one is also another pretty easy one. It's similar to another, but there are other little tricks!

### Operators

Locaical Operators:
```
&& , ||, !
```

Comparison operators:
```
> , < , >= , <= , ===, !==
```

### If statements
Example:
```
if(<condtional>){
    //do something
} else if (<conditional>) {
    ///do something else
} else {
    //really do something else 
}
```

### Short-circuit Evaluation
Example:
```
var hello = '';
var greeting (!hello) || 'fuck u';
```

### Ternary Operations
Example:
```
var locked = true;
locked ? console.log('Locked up') : console.log('Freedom');

var love = 'i love u';
love ? console.log('i love u too') : console.log('forever alone');
```

### Switch statements
Example:
```
var lovely = 'you are lovely';
switch(lovely) {
    case 'you are lovely':
        console.log('gimme love');
    case 'you are poopy':
        console.log('that is just not even nice');
    default:
        console.log('bye bye');
}
```

## Functions
Functions are pretty coo.

### Hoisting
JS allows hoisting, which allows access to function declarations before they are defined!

Example:
```
console.log(greeting());

function greeting() {
    console.log('hi hiiiiii');
}
```

### Passing Parameters to functons
Pretty simple, it looks like you dont have to declare types when passing in parameters. You can probably type cast them if you need to but otherwise i think it is okay if you just use the name. 

```
function sayHi(name) {
    console.log(`Hi there, ${name}`);
}

let name = 'Habeus';
sayHi(name);
```

Default parameters are pretty simple too. Simply assign values to the parameters!

```
function weNeedGuns(guns = 'Lots of guns') {
    console.log(`We need guns. ${guns}`)
}
```
### Returning values
Use the `return` keyword to return a value from a function.

```
function monitorCount(rows, columns) {
      return rows * columns;
}

const numOfMonitors = monitorCount(5, 4);
```
### Helper/Wrappers
JS allows for helper/wrapper functions (Function being called within another function).

```
function monitorCount(rows, columns) {
      return rows * columns;
}

function costOfMonitors(rows, columns) {
      return monitorCount(rows, columns) * 200;
}

const totalCost = costOfMonitors(5, 4);
```

### Function Expressions
We can also make function expressions. A function with no name is called and anonymous function. To invoke a function stored in a variable, call it like this `variableName(argument1, argument2, ...)`. THESE ARE NOT HOISTED!

```
const plantNeedsWater = function(day) {
      return day === 'Wednesday' || false;
};

plantNeedsWater('Tuesday');
```

### Arrow Syntax
Arrow syntax is trendy as shit. It's basically just a shorter way to define a function.

```
const plantNeedsWater = (day) => {
    return day === 'Wednesday' || false;
};

const plantNeedsWater('Wednesday');
```

### Concise Body Arrow Functions
A lot can be done to make arrow syntax more concise! Since JS isn't a compiled language, it is important to cut down on your code's "foot print".
Examples:
```
const plantNeedsWater = day =>
    day === 'Wednesday' ? true : false;

const randomFunct = () => console.log('This is a random ass function');

const twoFriends = (f1, f2) => { 
    console.log(`These are my friends, ${f1} and ${f2}`);
};
```
## Scope
Defines where variables can be accessed or referenced. You know, global and local scope?

### Blocks and scope
A block of code, like in almost every other language, is enclosed in {}. Think of them as "structural markers" for your code.
```
const vertical = '37 inch';

const impressive = () => { // <- start of block
    const body = '6 foot 8';
    return `Let's talk about me, the ${body} frame, the ${vertical} vertical leap...`;
}; // <- end of block
```

### Global variables
Things that are globally scoped are often declared outside of blocks, and are intended to be accessed by any code in your program.

```
const friend1 = 'Mr. wood';
const friend2 = 'Mr. leblanc';

const myFriends = () => {
    return `These are my friends: ${friend1} and ${friend2}.`;
};
```

### Block scope
When a varibale or function is in block scope, it is only accessible from within that block.

```
const logVisibleLightWaves = () => {
      const lightWaves = 'Moonlight';
        console.log(lightWaves);
};

logVisibleLightWaves();
console.log(lightWaves); // <- This will throw an error, since lightWaves is only accisble from within the function
```

### Scope pollution
Having too many global variables can cause problems (jee wiz whoda thunk)! Any global variable you make goes to the `global namespace`(allows variable to be accessed from *ANYWHERE* from within the program). These global variables remain until the program finishes. 

*Scope pollution is when we have too many global variables that exist in the global namespace, or when we reuse variables across different scopes.* This is a problem becuase globally scoped variables can end up colliding with locally scoped variables. This can cause some *unexpected* behavior.

```
let stars = 'North Star';

const callMyNightSky = () => {
        stars = 'Sirius';
        return 'Night Sky: ' + satellite + ', ' + stars + ', ' + galaxy;
};

console.log(callMyNightSky());
console.log(stars);
```

### Practice good scoping
Block scope is powerful in JS. It allows the dev to define variables with precision, and not pollute the global namespace. If a variable does not need to exist outside a block - it shouldn't!

## Arrays
Data organization 101 here people. Arrays are simple in JS!

Each element in an array can store a different data type. 

```
let randomInfo = ['element', 96, true];
```

Arrays start at zero!

```
const hello = 'Hello World';
console.log(hello[6]); // <- returns 'W'
```

You can access and change elements of an array. Even if it is a `const` variable, arrays are mutable structures in JS. This will be further explained in the next section.

```
let groceryList = ['bread', 'tomatoes', 'milk'];

groceryList[1] = 'avocados';
```

As we know, variables declared with `const` CANNOT be reassigned, but we can still change the elements of the array since they are mutable in JS. This means we can change the contents of a `const` array, but CANNOT reasign a new array or a different value. 

```
let condiments = ['Ketchup', 'Mustard', 'Soy Sauce', 'Sriracha'];

const utensils = ['Fork', 'Knife', 'Chopsticks', 'Spork'];

condiments[0] = 'Mayo'; // <- Valid
condiments = ['Mayo']; // <- Valid

utensils[3] = 'Spoon'; // <- Valid
utensils = ['2']; // <- Invalid; Will throw a TypeError
```

There are built-in operations for arrays. Below are some examples:

```
const objectives = ['Learn a new language', 'Read 52 books', 'Run a marathon'];

console.log(`${objectives.length}\n`); // <- getting length of the array

const hi = objectives.forEach((item, index, array) => {
      console.log(item, index);
}); // <- using forEach to iterate over the array

objectives.pop(); // <- Remove item from the END of the array
console.log(`\n${objectives}\n`);

objectives.push('Play Melee'); // <- Add item to the END of the array 
console.log(`${objectives}\n`); 

objectives.shift('Learn a new language'); // <- Remove item from the FRONT of the array
console.log(`${objectives}\n`);

objectives.unshift('Find Jub'); // <- Add item to the FRONT of the array
console.log(`${objectives}\n`);

const indexOfMelee = objectives.indexOf('Play Melee'); // <- Get index of item with  matching string
console.log(`${indexOfMelee}\n`);

consnt n = 1;
const removedItem = objectives.splice(indexOfMelee, n); // <- Remove an item; indexOfMelee is the starting point of the array and progressing towards the end of the array; n is the number of items to be removed
console.log(`${removedItem}\n`);

const start = objectives.indexOf('Learn a language');
const end = objectives.indexOf('Play Melee') + 1;
console.log(objectives.slice(start, end); // <- returns a shallow copy of the range provided

let shallowCopy = objectives.splice();
console.log(`${shallowCopy}`);

const removeItem = (arr) => { // <- Operating arrays using functions
    arr.pop();
};
removeItem(objectives);
console.log(`${objectives}`);

const numberClusters = [[1,2], [3,4], [5,6]]; // <- Creating and accessing nested arrays
const target = numberClusters[2][1];

let board = [ 
['R','N','B','Q','K','B','N','R'],
['P','P','P','P','P','P','P','P'],
[' ',' ',' ',' ',' ',' ',' ',' '],
[' ',' ',' ',' ',' ',' ',' ',' '],
[' ',' ',' ',' ',' ',' ',' ',' '],
[' ',' ',' ',' ',' ',' ',' ',' '],
[' ',' ',' ',' ',' ',' ',' ',' '],
['p','p','p','p','p','p','p','p'],
['r','n','b','q','k','b','n','r'] ];

console.log(board.join('\n') + '\n\n');

// Move King's Pawn forward 2
board[4][4] = board[6][4];
board[6][4] = ' ';
console.log(board.join('\n')); // <- Joins all elements of the array into one string
```
## Loops
Loops, man... You gotta use loops! Everyone loves loops! C'mon you know about loops, they keep us from manually repeating tasks!

### For Loops
Heh, the classic for loop. A most powerful tool. Very similar structure to for loops in other languages.

```
for(let counter = 5; counter <= 10; counter++) {
    console.log(counter);
}
```

So easy right?

We can loop through structures too!

```
const vacationSpots = ['Bali', 'Paris', 'Tulum'];

for(let counter = 0; counter < vacationSpots.length; counter++) {
      console.log(`I would love to visit ${vacationSpots[counter]}`);
}
```

### Nested Loops
You can nest deez loops too bruh, don't worry.

```
let bobsFollowers = [
  'Davy',
  'Travy',
  'Wavy',
  'Navy'
];

let tinasFollowers = [
  'Davy',
  'Lavy',
  'Navy'
];

let mutualFollowers = [];

for(var outer of bobsFollowers) {
  for(var inner of tinasFollowers) {
    if(outer === inner) {
      console.log(`True`);
      mutualFollowers.push(inner);
    }
  }
}

console.log(mutualFollowers);
```

### While Loops
The `while` loop. Another classic. Truly, one for the books.

```
const cards = ['diamond', 'spade', 'heart', 'club'];

let currentCard = '';

while(currentCard !== 'spade') {
  currentCard = cards[Math.floor(Math.random() * 4)];
  console.log(currentCard);
}
```

### Do...While Loops
Very similar to a `while` loop, BUT the do gaurantees the loop to run at least once!

```
const gamesOfMeleeToPlay = 100000;
let gamesPlayed = 0;

do {
    console.log('Reeeaaaady... GO!');
    gamesPlayed++;
} while (gamesPlayed < gamesOfMeleeToPlay);
```

### Break/Continue keywords
Similar to other languages, JS lets you use certain keywords to `break` or `continue` loops.

Example using `break`

```
const rapperArray = ["Lil' Kim", "Jay-Z", "Notorious B.I.G.", "Tupac"];

for(let counter = 0; counter < rapperArray.length; counter++) {
  if(rapperArray[counter] === 'Notorious B.I.G.') {
    console.log(rapperArray[counter]);
    console.log(`And if you don't know, now you know.`);
    break;
  }
}
```

Example using `continue`

```
for (let counter = 1; counter <= 10; counter++){
  if (counter % 2 == 1) { continue; }
  console.log(counter);
}
```

## Higher-Order Functions
Higher-order functions are functions that accept other functions as input. Think _AbStRaCtiOnS_. 

### Functions as Data
Functions behave like any other data type in the language, meaning they are _first class objects_.

They have properties such as `.length` and `.name` along with methods such as `.toString()`.

```
const checkThatTwoPlusTwoEqualsFourAMillionTimes = () => {
  for(let i = 1; i <= 1000000; i++) {
    if ( (2 + 2) != 4) {
      console.log('Something has gone very wrong :( ');
    }
  }
}

const is2p2 = checkThatTwoPlusTwoEqualsFourAMillionTimes;

console.log(is2p2.name); // <- will return the name of the assigned function to the console
```

### Functions as Parameters
A higher-order function is a fucntion that either accepts functions as parameters, returns a function, or both! 

We call the functions that get passed in as parameters and invoked _callback functions_ because they are _called_ during the execution of the higher-order function.

When we pass a function in as an argument to another function, we *do not* invoke it. Invoking the passed in function would evaluate to the return value of that call. With callbacks, we pass in the function itself by typing the function name *without* parentheses (*That would evaluate to the result of the calling function*):

```
const timeFuncRuntime = funcParameter => {
  let t1 = Date.now();
  funcParameter();
  let t2 = Date.now();
  return t2 - t1;
};

const addOneToOne = () => 1 + 1;

timeFuncRuntime(addOneToOne);
```

_Anotha one..._ This next one is a little extra but it gets this point across!

```
const addTwo = num => num + 2;

const checkConsistentOutput = (func, val) => {
    let call1 = func(val);
    let call2 = func(val);
    let result = call1 === call2 ? call1 : `This function returned inconsistent results`;
  return result;
};

const yeYe = checkConsistentOutput(addTwo, 2);

console.log(yeYe); // <- returns 4 to the console
```

## Iterators
Sometimes you want to iterate over the data found in one of your data structures. We use iterators to do this, silly.

There are some common built-ins that JavaScript has:
* `forEach()`
* `map()`
* `.filter()`

Examples on how to use these will be explained riiiiiight now...

### .forEach()
This method executes the same code for each element of an array. Pretty self explanitory yes? Good.

`.forEach()` takes an argument of _callback function_. (Remember? No? Go to the previous section!) During each execution, the current element is passed as an argument to the callback function.

The return value for `.forEach()` will always be `undefined`.

```
const fruits = ['mango', 'papaya', 'pineapple', 'apple'];

const eat = fruit => {
      console.log(`I want to eat a ${fruit}`);
};

fruits.forEach(eat);
```

### .map()
If you have ever programmed in Scala, this one should be very familiar to you.

`.map()` takes in an argument of a _callback function_ *BUT* returns a new array! So it is similar to the `.forEach()` but not quite the same.

```
const animals = ['Hen', 'elephant', 'llama', 'leopard', 'ostrich', 'Whale', 'octopus', 'rabbit', 'lion', 'dog'];

const secretMessage = animals.map(animal => {
  return animal[0]; // <- REMEMBER: Strings in most languages are just character arrays, so this works!
});

console.log(`${secretMessage}\n`);
console.log(`${secretMessage.join('')}\n`);


//Some simple division
const bigNumbers = [100, 200, 300, 400, 500];

const smallNumbers = bigNumbers.map(number => {
  return number / 100;
});
console.log(`${smallNumbers}`);
```

### .filter()
`.filter()` works very similar to `.map()`! It takes in a _callback function_, but returns an array where certain elements are filtered out.

The call back function passed to this method should return a `true` or `false`.

```
const randomNumbers = [375, 200, 3.14, 7, 13, 852];

const smallNumbers = randomNumbers.filter(number => {
  return number < 250;
});

console.log(`${smallNumbers}\n`);

const favoriteWords = ['nostalgia', 'hyperbole', 'fervent', 'esoteric', 'serene'];

const longFavoriteWords = favoriteWords.filter(word => {
  return word.length > 7;
});

console.log(`${longFavoriteWords}`);
```

### .findIndex()
`.findIndex()` returns the location of an element in an array. This method takes a _callback function_ as it's argument, and returns the index of the first element tat evaluates to `true`. 

Often, if an element is not found that matches the given condition, then a `-1` will be returned. 

```
const animals = ['hippo', 'tiger', 'lion', 'seal', 'cheetah', 'monkey', 'salamander', 'elephant'];

const foundAnimal = animals.findIndex(animal => {
  return (animal === 'elephant');
});

console.log(`Idx of elephant: ${foundAnimal}.\n`);

const startsWithS = animals.findIndex(animal => {
  return (animal[0] === 's');
});

console.log(`Idx of first animal that starts with s: ${foundAnimal}.`);
```

### .reduce()
`.reduce()` returns a single value after iterating through the elements of an array. 

`.reduce()` takes in a _callback function_, which takes in two parameters. These parameters are noramlly though of as an `accumulator` and a `currentValue`, which will be demonstrated in the following example.

`.reduce()` also takes in a second *optional* parameter which is... also an accumulator?... *BUUUUT* this second value acts an initial value for the accumulator!

```
const newNumbers = [1, 3, 5, 7];

const newSum = newNumbers.reduce((accumulator, currentValue) => {
  console.log(`The value of accumulator: ${accumulator}`);
  console.log(`The value of currentValue: ${currentValue}`);
  return accumulator + currentValue;
}, 10); // <- 10 is the starting value for the accumulator!

console.log(`${newSum}`);
```

### Using other built-ins
There are a lot of other built-in methods used to iterate over data. The following link will take you to the iterator documentation!

```
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array#Iteration_methods
```

Some examples of other built-ins...
```
const words = ['unique', 'uncanny', 'pique', 'oxymoron', 'guise'];

console.log(words.some(word => {
  return word.length < 6;
})); 

const interestingWords = words.filter(word => {
  return (word.length > 5);
});

const wordCheck = interestingWords.every(word => { 
  return (word.length > 5);
});

console.log(`\n${interestingWords}\n`);
console.log(`${wordCheck}`);
```

## Objects
JS objects are easy game player, don't worry! In JS we us curly braces to designate an _object literal_!

We fill an object with *unordered data*. The data is organized into _key-value pairs_, where the key is the variable name that points to a location in memory that holds the value. A key's value can be any data type in the language including functions OR other objects!

### Properties
Making this key value pair is easy my guy don't sweat it.

```
let spaceShip = {
    color: 'Red',
    'Fuel Type': 'Unobtainium' // <- fuel type needs quotes here because it contains a space!
};
```

You wanna access object properties!? I GOTCHU! You can just use _dot notation_ brother man!

```
let spaceship = {
  homePlanet: 'Earth',
  color: 'silver',
  'Fuel Type': 'Turbo Fuel',
  numCrew: 5,
  flightPath: ['Venus', 'Mars', 'Saturn']
};

let crewCount = spaceship.numCrew;
let planetArray = spaceship.flightPath;

console.log(crewCount);
console.log(planetArray);
```

### Bracket Notation
We can access a key's value using bracket notation too!

*YOU ABSOLUTELY MUST* use bracket notation when accessing keys that have numbers, spaces, or special characters.

With bracket notation you can also use a variable inside the brackets to select the keys of an object. This can be especially helpful when working with functions!

```
let spaceship = {
  'Fuel Type' : 'Turbo Fuel',
  'Active Mission' : true,
  homePlanet : 'Earth', 
  numCrew: 5
 };

let returnSpaceProp = (obj, prop) => obj[prop]; // <- for whatever reason if you use curly brackets to scope the fucntion body  it returns undefined... :thinking:

console.log(`${returnSpaceProp(spaceship, 'numCrew')}`);

let propName =  'Active Mission';
let isActive = spaceship[propName];

console.log(`${isActive}`);
```

### Property Assignment
Objects in JS are *mutable*! Do not forget this, we can update properties after we have created them!

You can use either _dot notation_ OR _bracket notation_ to update object properties! You can add properties using these notations too!

`delete` is an operator that allows us to remove properties from an object

```
let spaceship = {
  'Fuel Type' : 'Turbo Fuel',
  homePlanet : 'Earth',
  color: 'silver',
  'Secret Mission' : 'Discover life outside of Earth.'
};

Object.keys(spaceship).forEach(key => {
  console.log(key,':', spaceship[key]);
});

spaceship.color = 'glorious gold';

spaceship.numEngines = 4;

delete spaceship['Secret Mission'];

console.log('\nSecret Mission complete. Updating properties.\n');

Object.keys(spaceship).forEach(key => {
  console.log(key,':', spaceship[key]);
});
```

### Object methods
_Methods_ are functions that are stored in specific objects!

Look and see...

```
let retreatMessage = 'We no longer wish to conquer your planet. It is full of dogs, which we do not care for.';

let takeOffMessage = 'Spim... Borp... Glix... Blastoff!';

// Write your code below
let alienShip = {
  retreat: function () {
    console.log(`${retreatMessage}`);
  },
  takeOff: function () {
    console.log(`${takeOffMessage}`);
  }
};

alienShip.retreat();
console.log('\n');
alienShip.takeOff();
```

### Nesting Objects
This can make your code SUPER verbose it seems so pay very close attention to your object structure!

Accessing nested objects looks even more ugly so it is probably better to write functions that abstract over these messy calls, but for the sake of learning we'll do it the ugly way lol.

```
let spaceship = {
  passengers: [{
    name: 'Jacko'
  }],
  telescope: {
    yearBuilt: 2018,
    model: "91031-XLT",
    focalLength: 2032 
  },
  crew: {
    captain: { 
      name: 'Sandra', 
      degree: 'Computer Engineering', 
      encourageTeam() { console.log('We got this!') },
     'favorite foods': ['cookies', 'cakes', 'candy', 'spinach'] 
    },
  },
  engine: {
    model: "Nimbus2000"
  },
  nanoelectronics: {
    computer: {
      terabytes: 100,
      monitors: "HD"
    },
    'back-up': {
      battery: "Lithium",
      terabytes: 50
    }
  }
}; 

let capFave = spaceship.crew.captain['favorite foods'][0];

console.log(capFave);

let firstPassenger = spaceship.passengers[0];

console.log(firstPassenger);
```

### Pass By Reference
Objects are _passed by reference_ (*Think pointers in C++ my guy*). So if we were to pass in a variable assigned to an object into a function as a parameter, then that object gets permenantly mutated.

```
let spaceship = {
  'Fuel Type' : 'Turbo Fuel',
  homePlanet : 'Earth'
};

let greenEnergy = obj => { 
  obj['Fuel Type'] = 'avocado oil';
};

let remotelyDisable = obj => {
  obj.disabled = true; 
};

console.log(spaceship);
```

### Looping Through Objects
We can use `for...in` here! Attempting to access the index of an object would be difficult b/c the key-value pairs in objects are *NOT* ordered!

```
let spaceship = {
    crew: {
    captain: { 
        name: 'Lily', 
        degree: 'Computer Engineering', 
        cheerTeam() { console.log('You got this!') } 
        },
    'chief officer': { 
        name: 'Dan', 
        degree: 'Aerospace Engineering', 
        agree() { console.log('I agree, captain!') } 
        },
    medic: { 
        name: 'Clementine', 
        degree: 'Physics', 
        announce() { console.log(`Jets on!`) } },
    translator: {
        name: 'Shauna', 
        degree: 'Conservation Science', 
        powerFuel() { console.log('The tank is full!') } 
        }
    }
}; 

for(let crewMember in spaceship.crew) {
  console.log(`${crewMember}: ${spaceship.crew[crewMember].name}`);
};

for(let crewMember in spaceship.crew) {
  console.log(`${spaceship.crew[crewMember].name}: ${spaceship.crew[crewMember].degree}`)
}
```

## Advanced Objects
This section is for that little extra *OOMF* when looking at objects... 

### Getters
You know what a _getter_ is my guy. It is a method designed to access internal object properties. Sometimes you can use logic to check the property before you retrieve it.

Personally, I hate idea of getters and setters since i think it is a waste of typing, but hey you do you my guy. Unfortunately, in languages like Java and JavaScript these are _kind of_ unavoidable. You will see in my C++ notes what I'm talkin' about.

In JavaScript, we use the `get` keyword followed by a function and the _dot_ operator to call the function. remember to use `this` to access the property of the calling object. 

```
const robot = {
  _model: '1E78V2',
  _energyLevel: 10000000,
  get energyLevel() {
    if(typeof this._energyLevel === ('number')) {
      return `My current energy level is ${this._energyLevel}`;
    } else {
      return `System malfunction: cannot retrieve energy level`;
    }
  },
};

console.log(robot.energyLevel);
```

### Setters
Similar to getters. _setters_ use the `set` keyword. This method is designed to *assign/set* internal object properties.

In JavaScript, we *DO NOT* need to explicitly call the setter method. Syntactically, it will simply look like we are reassigning the value of an object property.

```
const robot = {
  _model: '1E78V2',
  _energyLevel: 100,
  _numOfSensors: 15,
  get numOfSensors(){
    if(typeof this._numOfSensors === 'number'){
      return this._numOfSensors;
    } else {
      return 'Sensors are currently down.'
    }
  },
  set numOfSensors(num) {
    if(typeof this._numOfSensors === ('number') && this._numOfSensors > 0){
      this._numOfSensors = num;
    } else {
      return 'Pass in a number that is greater than or equal to 0';
    }
  },
};

robot.numOfSensors = 100;

console.log(`${robot.numOfSensors}`);
```

### FaCtOrY FuNcTiOnS
Let's say we want to make a ton of the same object. Like user objects, player objects, w.e. We do this with _Factorie functions_. We can use *arrow syntax* to achieve this too!

Memes aside, this is a much better way to create and instantiate new objects comapred to what you have been shown so far in these notes.

```
const robotFactory = (model, mobile) => {
  return {
    model: model,
    mobile: mobile,
    beep() {
      console.log('Beep Boop');
    },
  };
};

const tinCan = robotFactory('P-500', true);

tinCan.beep();
```

### Property Value Shorthand
ES6 introduced some shortcuts for assigning properties to variables known as _destructing_.

Basically, it makes it so you can type less in the factory function.

```
function robotFactory(model, mobile){
  return {
    model, // <- refactored to shorthand
    mobile, // <- refactored to shorthand
    beep() {
      console.log('Beep Boop');
    }
  }
}

const newRobot = robotFactory('P-501', false)
console.log(newRobot.model)
console.log(newRobot.mobile)
```

### Destructed Assignment
We often want to extract key-value pairs from objects and save them as variables.

When using _destructed assignment_ we create a variable with the name of an object's key that is wrapped in curly braces `{ }` and assign it to the object.

```
const robot = {
  model: '1E78V2',
  energyLevel: 100,
  functionality: {
    beep() {
      console.log('Beep Boop');
    },
    fireLaser() {
      console.log('Pew Pew');
    },
  }
};

const { functionality } = robot; // <- functionality now references robot.functionality

functionality.beep();
functionality.fireLaser() // <- we can now call robot's functionality property and it's methods 
```

### Built-in Object Methods
Objects have built-in methods such `.hasOwnProperty()`, `.valueOf()`, and more! You can google them to find more on MDN's page!

The following are some more useful ones as well!

```
const robot = {
    model: 'SAL-1000',
  mobile: true,
  sentient: false,
  armor: 'Steel-plated',
  energyLevel: 75
};

const robotKeys = Object.keys(robot);
console.log(robotKeys);

const robotEntries = Object.entries(robot);
console.log(robotEntries);

let addFeature = {
  laserBlaster: true,
  voiceRecognition: true,
  };
const newRobot = Object.assign(addFeature, robot);
console.log(newRobot);
```

## Classes
OOP fundamentals people come on! We use classes to create new objects and define object behaviors _yata-yata_.

This will probably be a longer section so pay close attention and take your time!

### Constructors
There are a lot of similarities between `class` and `object` syntax in JavaScript: the _constructor_ method.

Everytime we want to instantiate a new class, JS calls the `constructor()` method.

```
class Gamer {
    constructor(name, consoleOfChoice) {
        this._name = name;
        this._consoleOfChoice = consoleOfChoice;
    }
}
```

### Instance
Once a class is defined, we can _instantiate_ a new object of that class. 

```
class Surgeon {
  constructor(name, department) {
    this.name = name;
    this.department = department;
  }
}

const surgeonCurry = new Surgeon(
  'Curry',
  'Cardiovascular'
);

const surgeonDurant = new Surgeon(
  'Durant',
  'Orthopedics'
);
```

### Methods
Classes can have member functions or _methods_. Most classes have getters and setters.

*Unlike* objects, you do not have to put commas between methods and variables.

```
class Surgeon {
  constructor(name, department) {
    this._name = name;
    this._department = department;
    this._remainingVacationDays = 20;
  }

  get name() {
    return this._name;
  }

  get department() {
    return this._department;
  }

  get remainingVacationDays() {
    return this._remainingVacationDays;
  }
  
  takeVacationDays(daysOff){
    this._remainingVacationDays = this._remainingVacationDays - daysOff;
  }
}

const surgeonCurry = new Surgeon('Curry', 'Cardiovascular');

surgeonCurry.takeVacationDays(4);

console.log(`${surgeonCurry.remainingVacationDays}`);
```

### Inheritance
OOP at it's finest my guy. _Inheritance_ is a tool that devs use to write less code. It allows the creation _child_ classes (subclasses) from _parent_ classes (super classes). Inheritance allows us to abstract the properties and methods found in a parent to it's children.

We use the _extends_ key word to signal that we want a class to inherit from a parent class. 

You have to call `super()` on the first line of the constuctor in a *child* class. If you don't, you will not be able to use `this` b/c the interpreter will throw a reference error.

It is considered best practice to call `super` on the first line of the constructor for each child class.

```
class HospitalEmployee {
  constructor(name) {
    this._name = name;
    this._remainingVacationDays = 20;
  }
  
  get name() {
    return this._name;
  }
  
  get remainingVacationDays() {
    return this._remainingVacationDays;
  }
  
  takeVacationDays(daysOff) {
    this._remainingVacationDays -= daysOff;
  }
}

class Nurse extends HospitalEmployee {
  constructor(name, certifications) {
    super(name);
    this._certifications = certifications;
    this._remainingVacationDays = 20;
  }

  addCertification(newCertification) {
    this._certifications.push(newCertification);
  }
}

const nurseOlynyk = new Nurse('Olynyk', ['Trauma', 'Pediatrics']);

nurseOlynyk.takeVacationDays(5);

nurseOlynyk.addCertification('Genetics');

for(const prop in nurseOlynyk) {
  console.log(`${prop} : ${nurseOlynyk[prop]}`);
}
```

Child classes also inherit methods from their parent as well!

For our above example, we do need to write a new `get name()` method for our `nurseOlynyk` object!

```
console.log(`${nurseOlynyk.name}`) //<- works just fine! no need to write a new getter
```

### Static Methods
In some cases, you want a class to have methods that aren't available in individual instances, but that you can call directly from the class!

Sound familiar? (If you know Java it should!) 

_Static_ methods allow you to call directly from the class, but not from the an instance of a class!

Static methods are often used to create utility functions for an application. Meaning static methods have *NO* access to data stored in specific objects!

Note that for static methods, the this keyword references the class. You can call a static method from another static method within the same class with this.

```
class HospitalEmployee {

.
.
.

  static generatePassword() {
      return Math.floor(Math.random() * 1000);
  }
}
```

## Browser Compatibility and Transpilation
Sometimes... things update... and other times... things don't update... BUT when things DO update you may need to update your code so it is compatible with things!

There are two resources that will save your ass:
*caniuse.com — A website that provides data on web browser compatibility for HTML, CSS, and JavaScript features. You will learn how to use it to look up ES6 feature support.
*Babel — A Javascript library that you can use to convert new, unsupported JavaScript (ES6), into an older version (ES5) that is recognized by most modern browsers.

While ~98% of features in ES5 are supported by browsers and only 90% of features i ES6 are supported by browsers, why are we not learning ES5 instead?

*Readability and economy of code — The new syntax is often easier to understand (more readable) and requires fewer characters to create the same functionality (economy of code).
*Addresses sources of ES5 bugs — Some ES5 syntax led to common bugs. With ES6, Ecma introduced syntax that mitigates some of the most common pitfalls.
*A similarity to other programming languages — JavaScript ES6 is syntactically more similar to other object-oriented programming languages. This leads to less friction when experienced, non-JavaScript developers want to learn JavaScript.

### Using npm
You start a JavaScript project with the following commands... don't worry I'll explain them too!

In this case, we are setting up a project that will also be able to transpile code using _Babel_.

To start, you need to initialize a project directory. Running `npm init` in your project directory will define your _project root_. This is where your _package.json_ file will live. 

```
$ npm init
```

A _package.json_ file contains info about your project:
* Metadata - Title, desc, authors, etc.
* A list of node packages required for the project - If another dev wants to work/run your project, then npm will reads the package.json file and downloads the dependcies
* Key-value pairs for cmd line scripts - You can use npm to run these shorthand scripts to perform some process.

Now, we're ready to install Babel! We will use `install` which creates a _node_modules_ directory and copies package files to it.
```
$ npm install babel-cli -D
$ npm install babel-preset-env -D
```
The `-D` flag instructs npm to add each package to a property called `devDependencies`, which is useful for other devs! It allows others to work with your project without having to manually install each package separately.

You can specify the initial JavaScript version inside of a file named .babelrc. In your root directory, you can run touch .babelrc to create this file. Usually, you want to transpile JavaScript code from versions ES6 and later (ES6+) to ES5. 

In the project root, we need a *.babelrc* file.
```
$ touch .babelrc
```
To specify that we are transpiling code from an ES6+ source, we have to add the following JavaScript Object to the *.babelrc* file.
```
{
      "presets": ["env"]
}
```

Now, I promise there is only one more step before we can transpile our code... swear to god... 

We need to specify a script in package.json that initiates the ES6+ to ES5 transpilation. 

Inside of the package.json file, there is a property named "scripts" that holds an object for specifying command line shortcuts.

We will a new script shortcut below the `test` shortcut. This calls babel, tells it to transpile all the code in the *src* dir, and instructs babel to write the transpiled code to a directory called *lib*. 
```
"build": "babel src -d lib"
```
Now we can call our new *build* script with the following:
```
npm run build
```
And boom, just like that you got some cross compatible code.

## Modules
Modules are kind of like _packages_ or _#include_ files found in Java and C++ respectively.

Modules... 
* make things easier to find, fix, and debug code.
* allow devs to reuse and recycle defined logic
* keep info protected/private from other modules if needed
* (IMPORTANT) keep us from polluting the global namespace and potential naming collisions, by cautiously selecting variables and behavior we load into a program

### module.exports
We'll start with Node.js, using the `module.exports` syntax.

Every JavaScript file run in Node has a local `module` object with an `exports` property used to define what should be exported from the file.

```
const Airplane = {};

Airplane.myAirplane = 'StarJet';

module.exports = Airplane;

console.log(module.exports);
```

It's just that easy my friend. 

### require()
If you want to make use of an exported module, then you will need to use the `require()` function to import modules. 

`require()` takes a file path as an argument pointing to the original module file.

*Assume that the above code is in a file called 1-airplane.js*

```
const Airplane = require('./1-airplane.js');

const displayAirplane = () => {
      console.log(Airplane.myAirplane)
}

displayAirplane();
```

### module.exports (contd.)
Note that we can wrap any collection of data and functions in an object, and export that object using `module.exports`.

*2-airplane.js*
```
module.exports = {
  myAirplane: "CloudJet",
  displayAirplane: function() {
    return this.myAirplane;
  },
};
```

*2-missionControl.js*
```
const Airplane = require('./2-airplane.js');

console.log(Airplane.displayAirplane());
```

### export default
As of ES6, JavaScript supports a more _readable_ and _flexible_ syntax for exporting modules: _default export_ and _named_ exports.

*airplane.js*
```
const Airplane = {};

Airplane.availableAirplanes = [
  {
    name: 'AeroJet',
    fuelCapacity: 800,
  },
  {
    name: 'SkyJet',
    fuelCapacity: 500,
  },
];

export default Airplane;
```

### import
ES6 modulke syntax now supports an `import` keyword that allows us to imort objects. 

*missionControl.js*
```
import Airplane from './airplane';

const displayFuelCapacity = () => {
  Airplane.availableAirplanes.forEach(function(element) {
    console.log(`Fuel Capacity of ${element.name} : ${element.fuelCapacity}`)
  });
};

displayFuelCapacity();
```

### Named Exports
_Named exports_ allow us to export data through the use of variables.

*airplane.js*
```
let availableAirplanes = [
  {
    name: 'AeroJet',
    fuelCapacity: 800,
    availableStaff: [
      'pilots',
      'flightAttendants',
      'engineers',
      'medicalAssistance',
      'sensorOperators',
    ],
  },
  {
    name: 'SkyJet',
    fuelCapacity: 500,
    availableStaff: [
      'pilots',
      'flightAttendants',
    ],
  },
];

let flightRequirements = {
  requiredStaff: 4 
};

const meetsStaffRequirements = (availableStaff, requiredStaff) => {
  if(availableStaff.length >= requiredStaff) {
    return true;
  } else {
    return false;
  }
};

export { availableAirplanes, flightRequirements, meetsStaffRequirements };
```

### Named Imports
Now... using _named imports_...

```
import { availableAirplanes, flightRequirements,meetsStaffRequirements } from './airplane';

const displayFuelCapacity = () => {
  availableAirplanes.forEach(function(element) {
    console.log(`Fuel Capacity of ${element.name} : ${element.fuelCapacity}`)
  });
};

function displayStaffStatus() {
  availableAirplanes.forEach(function(element){
    console.log(`${element.name} meets staff requirements ${meetsStaffRequirements(element.availableStaff, flightRequirements.requiredStaff)}`);
  });
}
displayStaffStatus();
displayFuelCapacity();
```

### Exports Named Imports
Named exports can exported as soon as they are declared by placing the `export` keyword in front of the variable declarations.

*updated airplane.js*
```
export let availableAirplanes = [
  {
    name: 'AeroJet',
    fuelCapacity: 800,
    availableStaff: [
      'pilots',
      'flightAttendants',
      'engineers',
      'medicalAssistance',
      'sensorOperators',
    ],
    maxSpeed: 1200,
    minSpeed: 300,
  },
  {
    name: 'SkyJet',
    fuelCapacity: 500,
    availableStaff: [
      'pilots',
      'flightAttendants',
    ],
    maxSpeed: 800,
    minSpeed: 200,
  },
];

export let flightRequirements = {
  requiredStaff: 4,
  requiredSpeedRange: 700, 
};

export let meetsSpeedRangeRequirements = (maxSpeed, minSpeed, requiredSpeedRange) => {
  const range = maxSpeed - minSpeed;
  if(range > requiredSpeedRange) {
    return true;
  } else {
    return false;
  }
}

export const meetsStaffRequirements = (availableStaff, requiredStaff) => {
  if(availableStaff.length >= requiredStaff) {
    return true;
  } else {
    return false;
  }
```

### Import Named Imports
Now we can update *missionControl.js* to use our updated *airplane.js* code!

```
import { availableAirplanes, flightRequirements,meetsStaffRequirements, meetsSpeedRangeRequirements } from './airplane';

const displayFuelCapacity = () => {
  availableAirplanes.forEach(function(element) {
    console.log(`Fuel Capacity of ${element.name} : ${element.fuelCapacity}`)
  });
};

function displayStaffStatus() {
  availableAirplanes.forEach(function(element){
    console.log(`${element.name} meets staff requirements ${meetsStaffRequirements(element.availableStaff, flightRequirements.requiredStaff)}`);
  });
}

function displaySpeedRangeStatus() {
  availableAirplanes.forEach(function(element) {
    console.log(element.name + 'meets speed range requirements: ' + meetsSpeedRangeRequirements(element.maxSpeed, element.minSpeed, flightRequirements.requiredSpeedRange));
  });
}

displayStaffStatus();
displayFuelCapacity();
displaySpeedRangeStatus();
```

### Export as
Named exports also conveniently offer a way to change the name of variables when we export or import them. We can do this with the `as` keyword.

Removing the `export` keywords from the front of each variable in *airplane.js*, we then append the following code snippet

```
export { availableAirplanes as aircrafts, flightRequirements as flightReqs, meetsStaffRequirements as meetsStaffReqs, meetsSpeedRangeRequirements as meetsSpeedRangeReqs };
```

### Import as
Now, OF COURSE we need to update *missionControl.js* my dude

```
import { aircrafts, flightReqs, meetsStaffReqs, meetsSpeedRangeReqs } from './airplane';

const displayFuelCapacity = () => {
  aircrafts.forEach(function(element) {
    console.log(`Fuel Capacity of ${element.name} : ${element.fuelCapacity}`)
  });
};

function displayStaffStatus() {
  aircrafts.forEach(function(element){
    console.log(`${element.name} meets staff requirements ${meetsStaffReqs(element.availableStaff, flightReqs.requiredStaff)}`);
  });
}

function displaySpeedRangeStatus() {
  aircrafts.forEach(function(element) {
    console.log(element.name + 'meets speed range requirements: ' + meetsSpeedRangeReqs(element.maxSpeed, element.minSpeed, flightReqs.requiredSpeedRange));
  });
}

displayStaffStatus();
displayFuelCapacity();
displaySpeedRangeStatus();
```

## Promises
A `Priomise` is a type of object that allows for _asynchronous_ operations.

Brief Refresher on Async Operations:
* An _asynchronous operation_ allows the computer to "move on" to other tasks while waiting for the async operation to to complete.

Easy right?

For web development, async operations are basically a requirement so this is a MUST KNOW MY GUY!

### Okay cool, but what EXACTLY is a Promise?
Promises are objects that represnt the eventual outcome of an asynchronous operation. 

A `Promise` object can be in one of three states:
* Pending: The initial state - operation has not yet completed
* Fulfilled: Operation has completed successfully and the promise has a _resolvedvalue_. 
	- Ex: A request's promise might be a JSON object as its value
* Rejected: Operation has failed and the promise has a reason for the failure.

### Constructing a Promise Object
Making a Promise is like instantiating any other object! We use the _new_ keyword my guy. 

The constructor for a `Promise` takes an executor function as a parameter, which is run automatically when the constructor is called. The executor _generally_ starts an asynchronous operation and dictates how the promise is settled.

*What is an executor function?*

An executor function takes two parameters:
* `resolve()`: a function with one argument. If invoked, it will change the promise's status from `pending` to `fulfilled`, and the value of the promise will be set to the argument passed into `resolve()`.
* `reject()`: a function that takes a reason or error as an argument. If invoked, it will change the promise's status from `pending` to `rejected`, and the promise's rejection reason will be set to the argument passed into `reject()`.

```
const inventory = {
      sunglasses: 1900,
        pants: 1088,
          bags: 1344
};

// Write your code below:
const myExecutor = (resolve, reject) => {
      if(inventory.sunglasses > 0) {
          resolve('Sunglasses order processed.');
      } else {
          reject('That item is sold out.');
      }
};

const orderSunglasses = () => {
      return new Promise(myExecutor);
};

let orderPromise = orderSunglasses();

console.log(orderPromise);
```

### Node setTimeout() Function
We need to know how to _consume_ a promise my guy. 

From here on, we can simulate the returned `Promise` object as the result of an asynchronous operation, by using the `setTimeout()` function from the Node API (a comparable API is provided by web browsers).

`setTimeout()` has two parameters:
* A callback function 
* A delay in milliseconds

The delay is performed asynchronously - the rest of the program won't stop executing during the delay of "at least" the time entered.

Async JavaScript uses something called the _event-loop_. After the your delay, the passed callback function is added to a line of code waiting to be run. *Before it can run*, any *synchronous* code from the program will run next. This means that it might take more than our delay time for our callback function to execute.

```
console.log("This is the first line of code in app.js.");
// Keep the line above as the first line of code
// Write your code here:
const usingSTO = () => {
      console.log("This is coming from usingSTO");
};

const returnPromise = () => {
      return new Promise((resolve, reject) => {
              setTimeout(usingSTO, 2000);
                });
}

returnPromise();

// Keep the line below as the last line of code:
console.log("This is the last line of code in app.js.");
```

### Consuming Promises
The initial state of a promise is `pending`, but we have a gaurentee that it will settle. 

*How do we tell the computer what should happen then?*

`Promise` objects come with a `.then()` method.

`.then()` is a higher-order function that takes two callback functions as arguments. We refer to these as _handlers_. 

* The first handler, sometimes called `onFulfilled`, is a _success handler_, and it should contain logic for resolving the promise.
* The second handler, sometimes called `onRejected`, is a _failure handler_, and it should contain the logic for the promise rejecting.

We can invoke `.then()` with one, both, or neither of the handlers! This allows for flexibility, but can also make for tricky debugging.

If the appropriate handler is not provided, instead of throwing an error, .then() will just return a promise with the same settled value as the promise it was called on. One important feature of .then() is that it always returns a promise.

### onFulfilled and onRejected Functions
We need to be able the _handle_ a successful promise. For this we use the `.then()` function.

We invoke `.then()` on the promise, passing in a _success handler_ callback function!

With typical promise consumption, we have no idea whether a promise will resolve or reject. This is why it is normal to provide the logic to handle both cases. We can pass both an `onFulfilled` and `onRejected` callback to `.then()`.

The below example is a lot so lemme break this down for you...

In *library.js* we have a function that we are exporting called `checkInventory()`. If every item is in stock, we return a success message. If not, we return a failure message. We use `setTimeout()` to ensure that the `checkInventory()` promise settles asynchronously.

*library.js*
```
const inventory = {
    sunglasses: 1900,
    pants: 1088,
    bags: 1344
};

const checkInventory = (order) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let inStock = order.every(item => inventory[item[0]] >= item[1]);
            if (inStock) {
                resolve(`Thank you. Your order was successful.`);
            } else {
                reject(`We're sorry. Your order could not be completed because some items are sold out.`);
            }
        }, 1000);
    })
};

module.exports = { checkInventory };
```

We write a `handleSuccess()` and a `handleFailure()` function that will act as our callback functions to pass into `.then()`

*app.js*
```
const {checkInventory} = require('./library.js');

const order = [['sunglasses', 1], ['bags', 2]];

// Write your code below:

const handleSuccess= resolvedValue => {
  console.log(`${resolvedValue}`);
};

const handleFailure = resolvedValue => {
  console.log(`${resolvedValue}`);
};

checkInventory(order).then(handleSuccess, handleFailure);
```

### Using catch() with Promises
We want to write _ClEaN cOdE_ so we will use a practice called _separation of concerns_. Separation of concerns means organizing our code into distinct sections, where each section handles a specific task. 

REMEBER: `.then()` will return a promise with the same settled value as the promise it was called on if no appropiate handler was provided. With this in mind, we can chain our `.then()` calls to use one handler at a time, instead of both at once... This will make sense in a second...

HOWEVER, instead of using a chain of `.then()`s, we can use a `.catch()`. This accomplishes the same thing as a `.then()` but takes only a failure handler.

In the following code, we are going to refactor app.js from the previous example.

```
const {checkInventory} = require('./library.js');

const order = [['sunglasses', 1], ['bags', 2]];

const handleSuccess = (resolvedValue) => {
  console.log(resolvedValue);
};

const handleFailure = (rejectReason) => {
  console.log(rejectReason);
};

// Write your code below:

checkInventory(order)
  .then(handleSuccess)
  .catch(handleFailure);
```

### Chaining Multiple Promises
One common pattern we’ll see with asynchronous programming is multiple operations which depend on each other to execute or that must be executed in a certain order. We might make one request to a database and use the data returned to us to make another request and so on!

The process of chaining promises together is called _composition_. Promises are designed with composition in mind!

The following example can be a little hard to follow so let daddy explain... 

`checkInventory()` expects an order argument and returns a promise. If there are enough items to fill the order, then the promise will resolve to an array. The first element in the resolved value array will be the same as `order` and the second element will be the total cost of the order as a number.

`processPayment()` expects an array argument with the `order` as the first element and the purchase total as the second. This function returns a promise. If there is a large enough balance on the giftcard associated with the order, it will resolve to an array. The first element in the resolved valuie array will be the same `order` and the second element will be a tracking number.

`shipOrder()` expects an array argument with the `order` as the first element and a tracking number as the second. It returns a promise which resolves to a string confirming the order has shipped.

The following *library.js* code is the example of this...

*library.js*
```
const store = {
  sunglasses: {
    inventory: 817, 
    cost: 9.99
  },
  pants: {
    inventory: 236, 
    cost: 7.99
  },
  bags: {
    inventory: 17, 
    cost: 12.99
  }
};

const checkInventory = (order) => {
  return new Promise ((resolve, reject) => {
   setTimeout(()=> {  
   const itemsArr = order.items;  
   let inStock = itemsArr.every(item => store[item[0]].inventory >= item[1]);
   
   if (inStock){
     let total = 0;   
     itemsArr.forEach(item => {
       total += item[1] * store[item[0]].cost
     });
     console.log(`All of the items are in stock. The total cost of the order is ${total}.`);
     resolve([order, total]);
   } else {
     reject(`The order could not be completed because some items are sold out.`);
   }     
}, generateRandomDelay());
 });
};

const processPayment = (responseArray) => {
  const order = responseArray[0];
  const total = responseArray[1];
  return new Promise ((resolve, reject) => {
   setTimeout(()=> {  
   let hasEnoughMoney = order.giftcardBalance >= total;
   // For simplicity we've omited a lot of functionality
   // If we were making more realistic code, we would want to update the giftcardBalance and the inventory
   if (hasEnoughMoney) {
     console.log(`Payment processed with giftcard. Generating shipping label.`);
     let trackingNum = generateTrackingNumber();
     resolve([order, trackingNum]);
   } else {
     reject(`Cannot process order: giftcard balance was insufficient.`);
   }
   
}, generateRandomDelay());
 });
};


const shipOrder = (responseArray) => {
  const order = responseArray[0];
  const trackingNum = responseArray[1];
  return new Promise ((resolve, reject) => {
   setTimeout(()=> {  
     resolve(`The order has been shipped. The tracking number is: ${trackingNum}.`);
}, generateRandomDelay());
 });
};


// This function generates a random number to serve as a "tracking number" on the shipping label. In real life this wouldn't be a random number
function generateTrackingNumber() {
  return Math.floor(Math.random() * 1000000);
}

// This function generates a random number to serve as delay in a setTimeout() since real asynchrnous operations take variable amounts of time
function generateRandomDelay() {
  return Math.floor(Math.random() * 2000);
}

module.exports = {checkInventory, processPayment, shipOrder};
```

*app.js*
```
const {checkInventory, processPayment, shipOrder} = require('./library.js');

const order = {
  items: [['sunglasses', 1], ['bags', 2]],
  giftcardBalance: 79.82
};

checkInventory(order)
.then((resolvedValueArray) => {
  // Write the correct return statement here:
 return processPayment(resolvedValueArray);
})
.then((resolvedValueArray) => {
  // Write the correct return statement here:
  return shipOrder(resolvedValueArray);
})
.then((successMessage) => {
  console.log(successMessage);
})
.catch((errorMessage) => {
  console.log(errorMessage);
});
```

NOW, if we type `node app.js` into the console, then we'll see the following output.

```
$ node app.js 
All of the items are in stock. The total cost of the order is 35.97.
Payment processed with giftcard. Generating shipping label.
The order has been shipped. The tracking number is: 150515.
```

### Avoiding Common Mistakes
*Mistake 1:* Nesting promises instead if chaining them!

Watch your brackets and indentations! 

*Mistake 2:* Forgetting to `return` a promise!

When chaining promises, you need to return the resolved promise so the next `.then()` has a value to operate on!

### Using Promise.all()
Now what if... bare with me... WHAT IF we don't care about the order of the execution of the promises?

For max efficiency we should use _concurrency_ or multiple asynchronous operations happening together. We promises, we can do this with the function `Promises.all()`

`Promises.all()` accepts an array of proimises as its argument and returns a single promise. That single promise will settle in one of two ways:

* If every promise in the argument array resolves, the single promise returned from `Promise.all()` will resolve with an array containing the resolve value from each promise in the argument array.
* If any promise from the argument array rejects, the single promise returned from `Promise.all()` will immediately reject with the reason that promise rejected. This behavior is sometimes called _failing fast_.

*library.js*
```
const checkAvailability = (itemName, distributorName) => {
    console.log(`Checking availability of ${itemName} at ${distributorName}...`);
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (restockSuccess()) {
                console.log(`${itemName} are in stock at ${distributorName}`)
                resolve(itemName);
            } else {
                reject(`Error: ${itemName} is unavailable from ${distributorName} at this time.`);
            }
        }, 1000);
    });
};

module.exports = { checkAvailability };


// This is a function that returns true 80% of the time
// We're using it to simulate a request to the distributor being successful this often
function restockSuccess() {
    return (Math.random() > .2);
}
```

*app.js*
```
const {checkAvailability} = require('./library.js');

const onFulfill = (itemsArray) => {
  console.log(`Items checked: ${itemsArray}`);
  console.log(`Every item was available from the distributor. Placing order now.`);
};

const onReject = (rejectionReason) => {
	console.log(rejectionReason);
};

// Write your code below:
const checkSunglasses = checkAvailability('sunglasses', 'Favorite Supply Co.');

const checkPants = checkAvailability('pants', 'Favorite Supply Co.');

const checkBags = checkAvailability('bags', 'Favorite Supply Co.');

const promiseArray = [checkSunglasses, checkPants, checkBags];

Promise.all(promiseArray)
  .then(onFulfill)
  .catch(onReject);
```

*output from console*
```
$ node app.js 
Checking availability of sunglasses at Favorite Supply Co....
Checking availability of pants at Favorite Supply Co....
Checking availability of bags at Favorite Supply Co....
sunglasses are in stock at Favorite Supply Co.
pants are in stock at Favorite Supply Co.
bags are in stock at Favorite Supply Co.
Items checked: sunglasses,pants,bags
Every item was available from the distributor. Placing order now.
```

## Async Await
As of ES6, _async...await_ is syntactic sugar for using promises to run asynchronous tasks. Basically, it should make your code much more readable because you don't have to spam `.then()` calls all over the place.

### The async keyword
The `async` keyword is used to write functions that handle asynchronous actions. We wrap our asynchronous logic inside a function prepended with the `async` keyword. Then, we invoke it.

```
const gamer = async () => {
    // insert very gamer asynchronous function logic here
};
gamer();
```
REMEMBER, `async` functions always return a promise.

This means we can use promise syntax! (`.then()`, `.catch`, etc.)

An `async` function will return in one of three ways:
* If there is nothing returned from the function, it will return a promise with a resolved value of `undefined`
* If there is a non-promise value returned from the function, it will return a promise resolved to the value
* If a promise is returned from the function, it will return that promise.
```
const withAsync = async (num) => {
      return num === 0 ? 'zero' : 'not zero';
}; 

console.log(withAsync(0));
console.log(withAsync(1));
```

### The await Operator
`async` functions alone don't really do much, which is why we need to use `await` too!

`await` can be used inside of an `async` function. 

`await` is an operator: it returns the resolved value of a promise. Since promises resolve in an indeterminate amount of time, `await` halts, or pauses, the execution of our `async` function until a given promise is resolved.

In *MOST* situations, we're dealing with promises that were returned from functions. Generally, these functions are through a library, meaning they are provided.

We can `await` the resolution of the promise it returns inside an `async` function. 

NOTE, the code example below uses a function called brainstormDinner(), which returns a promise!

*app.js*
```
async function announceDinner() {
    let result = await brainstormDinner();
	console.log(`I'm going to make ${result} for dinner.`); 
            
}

announceDinner();
```

### Writing async Functions
Okay okay okay, so we know that the `await` keyword halts the execution of an async function until a promise is no longer pending. 

Don't forget that `await` keyword!

It may seem obvious, but this can be a tricky mistake to catch because our function will still run... it just won't have the desired results...

Just watch...

*app.js*
```
const shopForBeans = require('./library.js');
function getBeans() {
  console.log(`1. Heading to the store to buy beans...`);
  let value = shopForBeans();
  console.log(`3. Great! I'm making ${value} beans for dinner tonight!`);
}

getBeans(); //This function call will return the wrong thing... why?
```

*app.js UPDATED TO USE ASYNC...AWAIT*
```
const shopForBeans = require('./library.js');

async function getBeans() {
  console.log(`1. Heading to the store to buy beans...`);
  let value = await shopForBeans();
  console.log(`3. Great! I'm making ${value} beans for dinner tonight!`);
}

getBeans();
```

### Handling Dependent Promises
The true power of `async...await` is when we have a series of asynchronous actions which depend on one another. 

An example of this might be, making a network request based on a query to a database. In this case, we would need to wait to make the network request unti we have the results from the database. 

We can use native promise syntax (`.then()`) OR we can make the code much more readable with `await`. 

The example below shows how we can avoid callback hell using await!

NOTE, the funcstions called in the code below come from a file called *library.js*. Each function returns a promise.

```
const {shopForBeans, soakTheBeans, cookTheBeans} = require('./library.js');

const makeBeans = async () => {
  let type = await shopForBeans();
  let isSoft = await soakTheBeans(type);
  let dinner = await cookTheBeans(isSoft);
  console.log(dinner);
};

makeBeans();
```

### Error Handling
Since `async...await` is syntactic sugar, we can still use `try...catch` blocks AND the native promise's `.catch()`!

NOTE, same as before, the code below uses functions defined in a *library.js* file. 

```
const cookBeanSouffle = require('./library.js');

const hostDinnerParty = async () => {
  try{
    let resolvedValue = await cookBeanSouffle();
    console.log(`${resolvedValue} is served!`);
  } catch (error) {
    console.log(`${error}`);
    console.log('Ordering a pizza!');
  }
};

hostDinnerParty();
```

### Handling Independent Promises
What happens if our `async` function contains multiple promises which are not dependent on the results of one another to execute?

In some cases it is better to take advantage of _concurrency_.

```
let {cookBeans, steamBroccoli, cookRice, bakeChicken} = require('./library.js')

const serveDinner = async () => {
  let vegetablePromise = steamBroccoli();
  let starchPromise = cookRice();
  let proteinPromise = bakeChicken();
  let sidePromise = cookBeans();
  console.log(`Dinner is served. We're having ${await vegetablePromise}, ${await starchPromise}, ${await proteinPromise}, and ${await sidePromise}.`);
};

serveDinner();
```

### Await Promise.all()
Another way to take advantage of _concurrency_ when we have multiple promises which can be executed simultaneously is to `await` a `Promise.all()`.

Promise.all() allows us to take advantage of asynchronicity— each of the four asynchronous tasks can process concurrently. Promise.all() also has the benefit of failing fast, meaning it won’t wait for the rest of the asynchronous actions to complete once any one has rejected. As soon as the first promise in the array rejects, the promise returned from Promise.all() will reject with that reason. As it was when working with native promises, Promise.all() is a good choice if multiple asynchronous tasks are all required, but none must wait for any other before executing.

```
let {cookBeans, steamBroccoli, cookRice, bakeChicken} = require('./library.js')

const serveDinnerAgain = async () => {
  let foodArray = await Promise.all([
    steamBroccoli(), 
    cookRice(), 
    bakeChicken(), 
    cookBeans(),
    ]);
  console.log(`Dinner is served. We're having ${foodArray[0]}, ${foodArray[1]}, ${foodArray[2]}, and ${foodArray[3]}.`);
};

serveDinnerAgain();
```

## Introduction to Requests
One of JavaScript's greatest assets is its non-blocking properties AKA _it is an asynchronous language_.

JavaScript uses an _event loop_ to handle asynchronous function calls. When a program runs, function calls are added to the stack. The functions that make requests that need to wait for servers to respond then get sent to a separate queue. Once the stack has cleared, THEN the functions in the queue are executed.

The heart of this section will be writing AJAX functions. _(Asynchronous JavaScript and XML)_

### XHR GET Requests
Asynchronous JavaScript and XML *(AJAX)*, enables requests to be made after the initial page load. 

The XMLHttpRequest(XHR) API, can be used to make many kinds of requests and supports other forms of data.

The following is boilerplate code that handles an AJAX GET request
```
// XMLHttpRequest GET
const xhr = new XMLHttpRequest(); //creates new xhr object
const url = 'https://api-to-call.com/endpoint'; //defines API endpoint

xhr.responseType = 'json'; //data format of response

xhr.onreadystatechange = () => { //this fuinction defines how to handle the response
      if(xhr.readyState === XMLHttpRequest.DONE) {
              return xhr.response;
                }
};

xhr.open('GET', url); //open request and send object
xhr.send();
```

Now that we know how to write some boilerplate AJAX code, we can write some code query the Datamuse API.

```
// Information to reach API
const url = 'https://api.datamuse.com/words?'; //api endpoint
const queryParams = 'rel_rhy=' //initial query that we can "build" onto

// Selecting page elements
const inputField = document.querySelector('#input');
const submit = document.querySelector('#submit');
const responseField = document.querySelector('#responseField');

// AJAX function
const getSuggestions = () => {
  const wordQuery = inputField.value; 
  const endpoint = `${url}${queryParams}${wordQuery}`; //built out query
  const xhr = new XMLHttpRequest();
  xhr.responseType = 'json';
  xhr.onreadystatechange = () => { //function defines how to handle the response
    if(xhr.readyState === XMLHttpRequest.DONE){
      renderResponse(xhr.response); //in this case, we are rendering the response data, which is to see what words rhyme with our input word
    }
  };
  xhr.open('GET', endpoint); //open GET request to endpoint
  xhr.send(); //send it, brother
}
```

So now we gotta talk about query strings homie... I know, I know... we all gotta do it.

A query string is separated from the URL using a `?` character. After `?`, you can then create a parameter which is a _key value pair_ joined by `=`. 

`'https://api.datamuse.com/words?key=value'`

If you want to add additional parameters, you will have to use the `&` character to separate your parameters.

`'https://api.datamuse.com/words?key=value&anotherKey=anotherValue'`

Let's try it...

```
// Information to reach API
const url = 'https://api.datamuse.com/words?';
const queryParams = 'rel_jjb=';
const additionalParams = '&topics=';

// Selecting page elements
const inputField = document.querySelector('#input');
const topicField = document.querySelector('#topic');
const submit = document.querySelector('#submit');
const responseField = document.querySelector('#responseField');

// AJAX function
const getSuggestions = () => {
  const wordQuery = inputField.value;
  const topicQuery = topicField.value;
  const endpoint = `${url}${queryParams}${wordQuery}${additionalParams}${topicQuery}`;
  
  const xhr = new XMLHttpRequest();
  xhr.responseType = 'json';

  xhr.onreadystatechange = () => {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      renderResponse(xhr.response);
    }
  }
  
  xhr.open('GET', endpoint);
  xhr.send();
};
```

### XHR POST Requests
The major difference between GET and POST is that a POST requires additional information to be sent through the request. This  additional info is sent in the _body_ of the post request.

```
const xhr = new XMLHttpRequest();
const url = 'https://api-to-call.com/endpoint';
const data = JSON.stringify({id: '200'}); //convert a value to a JSON string. This allows us to send data to the server

xhr.responseType = 'json';

xhr.onreadystatechange = () => { //AJAX function
  if(xhr.readyState === XMLHttpRequest.DONE) {
    return xhr.response();
  }
};

xhr.open('POST', url);
xhr.send(data);
```

*This next section requries a Rebrandly API key*

```
// Information to reach API
const apiKey = 'XXXXxxxxXXXXxxxxXXXXxxxxXXXXxxxx';
const url = 'https://api.rebrandly.com/v1/links';

// Some page elements
const inputField = document.querySelector('#input');
const shortenButton = document.querySelector('#shorten');
const responseField = document.querySelector('#responseField');

// AJAX functions
const shortenUrl = () => {
  const urlToShorten = inputField.value;
  const data = JSON.stringify({destination: urlToShorten});
  const xhr = new XMLHttpRequest();
  xhr.responseType = 'json';

  xhr.onreadystatechange = () => {
    if(xhr.readyState === XMLHttpRequest.DONE) {
      renderResponse(xhr.response);
    }
  };
  xhr.open('POST', url);

  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.setRequestHeader('apikey', apiKey);

  xhr.send(data);
};
```

## Requests Pt. 2
You thought we were done!?!?!? Heh... we're just getting started...

### fetch() GET Requests
`fetch()` info
* Creates a request object that contains relevant information that an API needs
* Sends that request object to the API endpoint provided
* Returns a promise that ultimately resolves to a response object, which contains the status of the primise with information the API sent back. 

```
fetch('https://api-to-call.com/endpoint').then((response) => {
      if(response.ok) { //boolean value that returns true if there are no errors
              return response.json(); //will return response data 
                }

                  throw new Error('Request failed!'); //will throw error if response.ok === false
}, (networkError) => {
      console.log(networkError.message);

}).then((jsonResponse) => {
      return jsonResponse; //return json response!

});
```

Using fetch in our datamuse API example.
```
const url = 'https://api.datamuse.com/words';
const queryParams = '?sl=';

// Selects page elements
const inputField = document.querySelector('#input');
const submit = document.querySelector('#submit');
const responseField = document.querySelector('#responseField');

// AJAX function
const getSuggestions = () => {
  const wordQuery = inputField.value;
  const endpoint = `${url}${queryParams}${wordQuery}`;
  
  fetch(endpoint, {cache: 'no-cache'}).then(response => {
    if (response.ok) {
      return response.json();
    }
    throw new Error('Request failed!');
  }, networkError => {
    console.log(networkError.message)
  }).then(jsonResponse => {
    renderResponse(jsonResponse);
  });
};
```

### fetch() POST Requests
Lets write a POST AJAX function using `fetch()`.
```
fetch('https://api-to-call.com/endpoint', {
  method: 'POST',
  body: JSON.stringify({id: '200'}),
}).then(response => {
  if(response.ok === true) {
    return response.json();
  }
  throw new Error('Request failed!');
}, networkError => {
  console.log(networkError.message);
}).then(jsonResponse => {
  return renderJsonResponse(jsonResponse);
});
```

Example using the Rebrandly API
```
// Information to reach API
const apiKey = 'XXXXxxxxXXXXxxxxXXXXxxxxXXXXxxxx';
const url = 'https://api.rebrandly.com/v1/links';

// Some page elements
const inputField = document.querySelector('#input');
const shortenButton = document.querySelector('#shorten');
const responseField = document.querySelector('#responseField');

// AJAX functions
const shortenUrl = () => {
  const urlToShorten = inputField.value;
  const data = JSON.stringify({destination: urlToShorten});
  fetch(url, {
    method: 'POST',
    headers: {
      'Content-type': 'application/json',
      'apikey': apiKey,
    },
    body: data,
  }).then(response => {
    if(response.ok === true){
      return response.json();
    }
    throw new Error('Request failed!'); 
  }, networkError => {
    console.log(networkError.message);
  }).then(jsonResponse => {
    renderResponse(jsonResponse);
  });
};
```
### async GET Requests
We can make ALL of the things we did earlier a lot easier using `async...await`!

```
const getData = async(url) => {
  try {
    const response = await fetch('https://api-to-call.com/endpoint');
    if(response.ok === true) {
      const jsonResponse = await response.json();
      return jsonResponse;
    }
    throw new Error('Request failed!');
  } catch (error) {
    console.log(error);
  }
};
```

Lets make an `async...await` AJAX function with the Datamuse API
```
// Information to reach API
const url = 'https://api.datamuse.com/words?';
const queryParams = 'rel_jja=';

// Selecting page elements
const inputField = document.querySelector('#input');
const submit = document.querySelector('#submit');
const responseField = document.querySelector('#responseField');

// AJAX function
const getSuggestions = async () => {
  const wordQuery = inputField.value;
  const endpoint = `${url}${queryParams}${wordQuery}`;
  try {
    const response = await fetch(endpoint, {cache: 'no-cache'});
    if(response.ok === true) {
      const jsonResponse = await response.json();
      renderResponse(jsonResponse);
    }
  } catch(error) {
    console.log(error);
  }
};
```

### async POST Requests
Now that you’ve made an async GET request, let’s start on getting you familiar the async POST request.

We still have the same structure of using try and catch as before. But, in the fetch() call, we now have to include an additional argument that contains more information like method and body.

```
const getData = async () => {
  try {
    const response = await fetch('https://api-to-call.com/endpoint', {
      method: 'POST',
      body: JSON.stringify({id: 200}),
    });
    if(response.ok) {
      const jsonResponse = await response.json();
      return jsonResponse;
    }
    throw new Error('Request failed!');
  } catch (error) {
    console.log(error);
  }
};
```

NOW, lets try doing this with the Rebrandly API!
```
// information to reach API
const apiKey = 'XXXXxxxxXXXXxxxxXXXXxxxxXXXXxxxx';
const url = 'https://api.rebrandly.com/v1/links';

// Some page elements
const inputField = document.querySelector('#input');
const shortenButton = document.querySelector('#shorten');
const responseField = document.querySelector('#responseField');

// AJAX functions
const shortenUrl = async () => {
  const urlToShorten = inputField.value;
  const data = JSON.stringify({destination: urlToShorten});

  try {
    const response = await fetch(url, {
      method: 'POST',
      body: data,
      headers: {
        'Content-type' : 'application/json',
        'apikey' : apiKey,
      },
    });
    if(response.ok === true) {
      const jsonResponse = await response.json();
      renderResponse(jsonResponse);
    }
    throw new Error('Request failed!');
  } catch(error) {
    console.log(error);
  }
};
```
Congratulations on learning all about AJAX requests using fetch(), async, and await! These concepts are fundamental and will help you develop more robust web apps!
