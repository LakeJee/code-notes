# Python Notes

Similar to `js_notes`, these notes are coming from codecademy! Unfortunately, the course only covers Python 2 (if you want Python 3, you have to upgrade to PRO...).

REGARDLESS, this is a good enough opportunity to the learn the fundamentals of Python. Maybe at the very end of the notes I'll add some key differences between the two versions, but for now keep in mind we are learning Python 2.

# Before we start... 

Below are some useful links!
* https://www.codecademy.com/learn/learn-python
* https://ehmatthes.github.io/pcc/cheatsheets/README.html
* TBA...

## Basics

### Output

In Python, printing output is easy! Simply type `print` followed by a string.
```
print 'Hello snek'
```

Python 2's print statements allow for concatination using the `+` symbol too
```
print 'Hello ' + 'snek'
```

### Variable syntax

Python is a _dynamically typed_ language. This means, when declaring variables, we DO NOT need to explicitly declare the types of data we are assigning.
```
hello_variable = 'Hello fellow homies'
print '%s' % hello_variable
```
Above, we can see that we didn't need to put a `String` in front of the variable name becuase Python's interpreter will handle that for us!

We can also see some less than intuitive string interpolation as well, if you're used to C syntax, then this is a normal style for you!

### Arithmetic

Math is ezpz lemon sqeezy in Python!

```
product = 6 * 6
remainder = 1398 % 11

print '%d' % product
print '%d' % remainder
```

### Updating variables
There is no private data in Python. This means that variables can be updated as program execution progresses.

Now i know what you're thinking _"Oh no, what if I don't want data in an object to be accessed outside it's own class"_ ... Well, we can mimic the idea of _encapsulation_ and I'll show you how to do that later THE RIGHT WAY. For now, it's all public and everything can access everything.

```
january_to_june_rainfall = 1.93 + 0.71 + 3.53 + 3.41 + 3.69 + 4.50
annual_rainfall = january_to_june_rainfall

july_rainfall = 1.05
annual_rainfall += july_rainfall

august_rainfall = 4.91
annual_rainfall += august_rainfall

september_rainfall = 5.16
october_rainfall = 7.20
november_rainfall = 5.06
december_rainfall = 4.06

annual_rainfall += september_rainfall + october_rainfall + november_rainfall + december_rainfall
```

Above, you can see that we are updating `annual_rainfall` _after_ it has already been assigned values!

### Comments
Comments are easy in Python too! Just use a `#`*(POUND SYMBOL)*

```
# The below variable is very mysterious...
mystery_varliable = 'who am i'
```

### Numbers
In Python, the most simple kind of number is an `int`. Then there's a `float` which is similar to a `double`. 

`float`s can be written in scientific notation i.e. _1.5e2_.

```
cucumbers = 99
price_per_cucumber = 3.25

total_cost = price_per_cucumber * cucumbers

print total_cost
```

### Two Types of Division
When you divide two integers, you get an integer back. If the two numbers you're dividing do not divide evenly, then the decimal is trucated into an integer. If you need the precision, then you can cast a an `int` to `float` 

```
cucumbers = 100
num_people = 6

whole_cucumbers_per_person = cucumbers / num_people

float_cucumbers_per_person = float(cucumbers)/float(num_people)

print whole_cucumbers_per_person
print float_cucumbers_per_person
```

### Multi-line Strings
This is a pretty cool feature of Python. Making a multi-line string is easy!

You use open/close triple quotes (`"""<some long string>"""`) to create a multi-line string!
 
```
haiku = """The old pond,
A frog jumps in:
Plop!"""

print haiku
```

### Booleans
Your typical `True`/`False` logic!

```
gamer = True
fortnite = False
```

### ValueError
As mentioned earlier, Python is a _dynamically typed_ language. This means that the interpreter will automatically assign a datatype to a variable. 

Sometimes we want to change these datatypes...

```
float_1 = 0.25
float_2 = 40.0
product = float_1 * float_2

big_string = 'The product was %d' % product

print big_string # prints: The product was 10
```

## Strings & Console Output
We all know about strings. A `string` is a sequence of characters, numbers, and/or symbols. You can use double quotes `" "` or single quotes `' '` to enclose a string.

```
name = 'Julia'
brother = 'Andrew'
```

### Escaping characters
There are some characters that can cause problems in strings and they need to be _taken care of :eyes:_... BY ESCAPING THEM OF COUSE!

The way we escape a characters in Python is with a backslash `\`.

```
hello_friend = 'what\'s up my guy?'
```

Common things that need to be escaped are:
* single apostrophes -> \'
* back slash -> \\
* new line -> \n
* carriage return -> \r
* tab -> \t
* backspace -> \b
* octal value -> \ooo
    * 
    ```
        #A backslash followed by three integers will result in a octal value:
        txt = '\110\145\154\154\157'
        print txt
    ```
* hex value -> \xhh
    * 
    ```
        #A backslash followed by an 'x' and a hex number represents a hex value:
        txt = \x48\x65\x6c\x6c\x6f'
        print txt 
    ```
### Access by index
Python, like C and C++, stores `string`s as _character arrays_. This makes accessing the array easy! 

And remember!... ARRAYS START AT 0!

```
"""
The string "PYTHON" has six characters,
numbered 0 to 5, as shown below:

+---+---+---+---+---+---+
| P | Y | T | H | O | N |
+---+---+---+---+---+---+
  0   1   2   3   4   5

So if you wanted "Y", you could just type
"PYTHON"[1] (always start counting from 0!)
"""

monty = 'MONTY'
fifth_letter = monty[4]

print fifth_letter # prints "Y" to the console
```

### String methods 
How many string methods are there? Not a _TON_ but not a small amount either. For the purpose of this doc, we will focus on four of them! 

* `len()`: takes a string as an argument; returns length of string 
* `lower()`: string method; returns the same string in lowercase
* `upper()`: string method; returns the same string in uppercase 
* `str()`: takes non-string as an argument; returns input as string

If you want info on _EVERY_ string method, then look here (https://docs.python.org/2.5/lib/string-methods.html)

Lets test some functions shall we... 
```
parrot = 'Norwegian Blue'

print len(parrot) # prints "14" to the console
print parrot.lower() # prints "norwegian blue" to the console
print parrot.upper() # prints "NORWEGIAN BLUE" to the console
print str(pi) # prints "3.14" to the console
```

Why do some methods require a dot and some don't? That's where we get into the difference between methods and functions!

A _Method_ is a function that is defined in the class of the given object!

A _Function_ is a globally defined procedure!

SO, the `len()` and `str()` are functions that can operate on multiple datatypes, while `lower()` and `upper()` are defined within the `string` object!

### String Concatination
You knooooooow this one now! We use the `+` to concatinate strings! Think of it as _adding_ them together!

```
print 'Spam ' + 'and ' + 'eggs'
```

### String formating
We can format the strings that will be output to the console with the `%` symbol.

If you want to print an integer, you can _pad_ it with zeros! For example using `%02d` The `0` means _pad with zeros_, the `2` means to pad two characters wide, and the `d` means the number to be formatted is a _signed integer_.
```
string_1 = "Camelot"
string_2 = "place"

# Let's not go to Camelot. 'Tis a silly place. 
print 'Let's not go to %s. 'Tis a silly %s.' % (string_1, string_2)

day = 6
# 03 - 06 - 2019
print '03 - %02d - 2019' % (day)
```

## Date and Time
In life, you wanna know when things happen. When's your mom's birthday? When's the next election? When do I win the lottery? Well sometimes it's the same thing in software engineering!

Python's `datetime` library allows you to know this!

### Using datetime
This will be your first time looking at imports too!
```
from datetime import datetime

now = datetime.now()

print now # prints "2020-10-01 18:00:21.602469"
```

The import statement `from datetime import datetime` can be read as _"From the library called datetime, import the datetime methods"_

### Extracting information
Looking at the information we printed before, we can see that it's quite verbose. What if we wanted specific info? 
```
from datetime import datetime

now = datetime.now()

print now.year
print now.month
print now.day
```
Here we see `now.year`, `now.month`, and `now.day`... why are there no parentheses after the dot words? That's becuase they aren't methods! Here we are accessing parts of an object, so no parentheses needed!

### Hot date
What if we want to format a date? Hmmmmmm? We know how to format strings don't we!? 
```
from datetime import datetime
now = datetime.now()

print '%02d/%02d/%02d' % (now.month, now.day, now.year)
```

We can do hours too!
```
from datetime import datetime
now = datetime.now()

print '%02d:%02d:%04d' % (now.hour, now.minute, now.second)
```

## Conditionals & Control Flow
Well well well... Look what we have here... This dev wants to learn about control flow in python, huh? 

... 

WELL YOU'VE COME TO THE RIGHT PLACE!

### Boolean comparators and Logical Operators 
If you're used to Java, C++, C, and many other languages OTHER THAN JavaScript, then this is gonna look very familiar to you!

We got all the usual suspects to do boolean comparisons!
* `==` : _equal to_
* `!=` : _not equal to_
* `<`  : _less than_
* `<=` : _less than or equal to_
* `>`  : _greater than_
* `>=` : _greater than or equal to_

Lets also not forget about our friends the _logical operators_!
* `and` : _logical AND_
* `or`  : _logical OR_
* `not` : _logical NOT_

### Conditionals
You know these! These are ez pz I promise! Python's syntax is so simple, I won't even explain these that much! Why? Am I lazy? Yes... But also I believe *YOU*, yes *YOU*, will be able to understand these no problem! I believe in you!

`if/else` statements
```
isGamer = 'Yes'

if isGamer == 'Yes':
    print 'Hello fellow gamer. I knew you'd come...'
elif isGamer == 'Maybe':
    print 'Doesn't sound too convincing to me, but okay...'
else:
    print 'Looks you've been playing fortnite, huh?...'
```

The main thing that I want you to focus on is the spacing syntax. Python is an _indentation delimited_ language. This means that in order to format your blocks of code you need to pay attention to how you indent and space your code after certain lines.

We see an example of this in most cases where you use a semicolon `:`! In most cases, your editor will create this indent for you when you hit `Enter` after typing a semicolon, but always make sure it does it correctly! 

Lets look at one last example!
```
def grade_converter(grade):
    if grade >= 90:
        return "A"
    elif grade >= 80 and grade <= 89:
        return "B"
    elif grade >= 70 and grade <= 79:
        return "C"
    elif grade >= 65 and grade <= 69:
        return "D"
    else:
        return "F"
      
# This should print an "A"      
print grade_converter(92)

# This should print a "C"
print grade_converter(70)

# This should print an "F"
print grade_converter(61)
```

## PYGLATIN
Lets take a lil break and try something fun!

If you're looking for just a Python tutorial, then you can probably skip this section!

### Break it down
Pig Latin is a language game, where you move the first letter of the word to the end and add “ay.” So “Python” becomes “ythonpay.” To write a Pig Latin translator in Python, here are the steps we’ll need to take:
1. Ask the user to input a word in English
2. Make sure the user entered a valid word
3. Convert the word from English to Pig Latin
4. Display the translation result

### Heh, yup... it's code time...
Here, you will see our first use of `raw_input()`. This function accepts a string as input, prints it, and then waits for the user to type something into the console and press Enter/Return.

Here is an example:
```
hello = raw_input('Enter a word please: ')
print hello
```
In the abov example, the console will prompt the user for some input into the terminal window and then will print that input back to the user.

_What if we want some input validation?_

We can do that too!
```
hello = raw_input('Enter a word please: ')

if(len(hello) > 0):
    print hello
else: 
    print 'empty'
```

There we go! You can see in the above example that we check to make sure the length of the string is greater than zero. 

_Okay cool... but what if we want MOOOORE checking?_

...

You just can't be satisfied can you?... Okay well here you go...
```
hello = raw_input('Enter a word please: ')

if(len(hello) > 0 and hello.isalpha()):
  print hello
else: 
  print 'empty'
```

You happy yet!?!? Above, you can see that we also check to make sure that the user input contains only letters!

*NOTE:* The user input can also NOT contain spaces!

### [Py]glatin
Now we can start writing our translator!

```
pyg = 'ay'

original_word = raw_input('Enter a word:')

if len(original_word) > 0 and original_word.isalpha():
  word = original_word.lower()
  first_letter = word[0]
  new_word = word + first_letter + pyg
  new_word = new_word[1:len(new_word)]
  print 'Your new word in Pyglatin is: %s' % new_word
else:
  print 'empty'
```

Breaking the above example down. we can see some new things!

This line of code (`new_word = new_word[1:len(new_word)]`) is us _slicing_ `new_word` so that we don't print the first letter of the word!

So instead of returning this `Your new word in Pyglatin is: hellohay`, we will retunr this `Your new word in Pyglatin is: ellohay`!

## Functions
What good are functions? *CODE REUSE* my young padawan...

There are generally three components to a function:
1. The _header_, which includes the `def` keyword, the name of the function, and any _parameters_ the function might require.
	* `def hello_there_big_fella():`
2. An optional _comment_ that explains what the function does!
	* `"""Will print 'hello there big fella' to the console"""`
3. The _body_ which contains the logic of the function. The body *MUST* be _indented_!
	* print 'hello there big fella'

So lets put it together!
```
def spam():
  """Eggs incoming!"""
  print 'Eggs!'

spam()
```

### Call and Response
Lets look at as function that is _called_ with a parameter passed in!
```
def square(n):
  """Returns the square of a number."""
  squared = n ** 2
  print "%d squared is %d." % (n, squared)
  return squared

square(10)
```

In the above function, we see `n` in the function header. This is called a _parameter_. The `10` passed in when the function is called is known as an _argument_ (_The value(s) of the parameter(s) passed into a function_)

### Functions calling Functions
In python, we can make a function call another function! This is commonly refered to as _wrapping_ or _helping_!

```
def one_good_turn(n):
  return n + 1
    
def deserves_another(n):
  return one_good_turn(n + 2)
```

### I know Kung Fu
Remember `import`? Yeah... it's back!

When you `import` a _module_, you are yoinking in a a file that contains a function definitions that you can use!

Often, we only want to pull in certain functions and not entire modules! This is where the `from` keyword comes from! The syntax for this is as follows:
```
from module import function
```

REMEMBER THIS! You will be using this so much it's not even funny.
```
from math import sqrt

print sqrt(25) # <-- so we don't have to write math.sqrt(25)
```

Some cool things you can do is use the `as` keyword!
```
from math import sqrt as s

print s(13689) # <-- prints 117.0
```
### Universal Imports
If you *REALLY* want to pull in all of the functions from a file, use a `*`!

```
from math import *

print sqrt(25) 
```
_But sensei... is this a good idea?_

### HERE BE DRAGONS
Universal imports look great on the surface! Many of us first learn to program by importing entire libraries/modules into scope (`import java.util.*;`, `from math import *`, `using namespace std;`). But there's a huge problem with this...

Using this type of importing fills our program with a _ton_ if variable and function names without the saftey of those names still being associated with the module(s) they came from!

If you have custom written functions, and you are importing a bunch of other functions from a module, there may be a chance that your function names collide!

Even if your own definitions don't directly conflict with names from imported modules, if you import * from several modules at once, you won’t be able to figure out which variable or function came from where.

```
import math # Imports the math module
everything = dir(math) # Sets everything to a list of things from math
print everything # Prints 'em all!
```

### Built-Ins
Python comes with some built-ins so you don't always need to import a module!
```
maximum = max(3, 690, 0)

print maximum # <-- prints 690 

minimum = min(-123, 123123, -123123123123)

print minimum # <-- prints -123123123123

absolute = abs(-42)

print absolute # <-- prints 42

print type(22) # <-- <type 'int'>
print type(33.1) # <-- <type 'float'>
print type('allo') # <-- <type 'str'>

def distance_from_zero(n):
  if(type(n) == int or type(n) == float):
    return abs(n)
  else:
    return 'Nope'
```

## Taking a Vacation
This is just function practice! If you wanna good example, give this code a try! If not, you can skip this section!
```
def hotel_cost(nights):
  return 140 * nights

def plane_ride_cost(city):
  if(city == 'Charlotte'):
    return 183
  elif(city == 'Tampa'):
    return 220
  elif(city == 'Pittsburgh'):
    return 222
  elif(city == 'Los Angeles'):
    return 475

def rental_car_cost(days):
  total = days * 40
  if(days >= 7):
    total -= 50
  elif(days >= 3):
    total -= 20
  return total

def trip_cost(city, days, spending_money):
  return rental_car_cost(days) + hotel_cost(days - 1) + plane_ride_cost(city) + spending_money

print 'Total cost of a trip to LA: %.2f' % trip_cost('Los Angeles', 5, 600)
```

## Lists
Lists are _datatype_ that you can use to store a collection of different pieces of information as a sequence under a single variable name!

`example_list = [item_1, item_1]`

### Access by Index
Accessing a list is the same as accessing the index of string too! Gives us an intuitive way to iterate over and access elements in a list!

```
example_list = ['Andrew', 'Julia', 'Jake']

print 'Whaddup ' + example_list[0] # <-- prints 'Whaddup Andrew'
print 'Whaddup ' + example_list[1] # <-- prints 'Whaddup Julia'
print 'Whaddup ' + example_list[2] # <-- prints 'Whaddup Jake'
```

And don't you EVER forget... *Arrays/Lists/Vectors/etc. start at 0!*

### Making assignments
The structure of a list is mutable (meaning we can append elements to the end any time we like), *AND* the elements of that list are mutable!
```
zoo_animals = ['pangolin', 'cassowary', 'sloth', 'tiger']

zoo_animals[2] = 'hyena'

zoo_animals[3] = 'panda'
```

### Late arrivals and List lengths
As stated before, we can add elements to the end of a list at any time! We can also use the `len()` function on lists too!
```
suitcase = [] 
suitcase.append("sunglasses")

suitcase.append('shoes')
suitcase.append('tequila')
suitcase.append('cash')


list_length = len(suitcase) 

print "There are %d items in the suitcase." % (list_length)
print suitcase
```

### Slice it up
_But sensei, what if we only want to access certain portions of a list?_

Excellent question my friend...

When we want to access only certain portions of a List we do something called _slicing_. When you see the syntax of how to do this, the name makes a lot more sense!
```
suitcase = ["sunglasses", "hat", "passport", "laptop", "suit", "shoes"]

first = suitcase[0:2]
print first # <-- prints '['sunglasses', 'hat']'

middle = suitcase[2:4]
print middle # <-- prints '['passport', 'laptop']'

last =  suitcase[4:6]
print last # <-- prints '['suit', 'shoes']'
```

You can slice strings too!
```
animals = "catdogfrog"

cat = animals[:3]

dog = animals[3:6]

frog = animals[6:10]
```

### Maintaing Order
What if we want to get the index of certain items?

Lets see what happens when we want to insert an item into a list at a certain index!
```
nimals = ["aardvark", "badger", "duck", "emu", "fennec fox"]

duck_index = animals.index('duck')

animals.insert(duck_index, 'cobra')

print animals # <-- prints ['aardvark', 'badger', 'cobra', 'duck', 'emu', 'fennec fox']
```

### Looped out of our minds
We can loop through lists and operate on their elements!
```
my_list = [1,9,3,8,5,7]

for number in my_list:
  if(number % 2 == 0):
    print number
  else:
    print 'Not even'
```

```
start_list = [5, 3, 1, 2, 4]
square_list = []

for x in start_list:
  square_list.append(x ** 2)

square_list.sort()

print square_list
```

### Dictionaries
A dictionary is similar to a list, but you can access values by looking up a _key_ instead of an index. A _key_ can be any string or number!

There are many data structures with _key-value pairs_, but dictionaries are the most common in Python!
```
# Assigning a dictionary with three key-value pairs to residents:
residents = {'Puffin' : 104, 'Sloth' : 105, 'Burmese Python' : 106}

print residents['Puffin'] # Prints Puffin's room number

print residents['Sloth']
print residents['Burmese Python']
```

We can also add new entries to a dictionary too!
```
menu = {} # Empty dictionary
menu['Chicken Alfredo'] = 14.50 

menu['Salad'] = 12.12
menu['Veal'] = 4.20
menu['Pizza'] = 3.12

print "There are " + str(len(menu)) + " items on the menu."
print menu
```

Dictionaries are mutable, so we can change them in many many ways!

We can delete entries in a dictionary using the `del` keyword!
```
# key - animal_name : value - location 
zoo_animals = { 
	'Unicorn' : 'Cotton Candy House',
	'Sloth' : 'Rainforest Exhibit',
	'Bengal Tiger' : 'Jungle House',
	'Atlantic Puffin' : 'Arctic Exhibit',
	'Rockhopper Penguin' : 'Arctic Exhibit'
}

# Removing the 'Unicorn' entry. (Unicorns are incredibly expensive.)
del zoo_animals['Unicorn']

# Your code here!
del zoo_animals['Sloth']
del zoo_animals['Bengal Tiger']

zoo_animals['Rockhopper Penguin'] = 'Otter House' 

print zoo_animals
```

### Removing from a List
We can delete entries from a List using the `.remove()` function too!
```
backpack = ['xylophone', 'dagger', 'tent', 'bread loaf']

backpack.remove('dagger')
``` 

### The final test
You think you mastered Lists and Dictionaries? 
```
inventory = {
  'gold' : 500,
  'pouch' : ['flint', 'twine', 'gemstone'], # Assigned a new list to 'pouch' key
  'backpack' : ['xylophone','dagger', 'bedroll','bread loaf']
}

# Adding a key 'burlap bag' and assigning a list to it
inventory['burlap bag'] = ['apple', 'small ruby', 'three-toed sloth']

# Sorting the list found under the key 'pouch'
inventory['pouch'].sort() 

# Adding pocket to inventory and storing things in pocket
inventory['pocket'] = ['seashell', 'strange berry', 'lint']

# Sorting backpack
inventory['backpack'].sort()

# Removing dagger value from backpack
inventory['backpack'].remove('dagger')

# Giving the player 50 gold
inventory['gold'] += 50

# Show the new inventory! 
print inventory
```

## Prcatice, practice, practice
This is another practice section, so again feel free to skip to the next section!

### Print the items in a List
```
names = ["Adam","Alex","Mariah","Martine","Columbus"]

for n in names:
      print n
```

### Print the items in a Dictionary using keys
```
webster = {
  "Aardvark" : "A star of a popular children's cartoon show.",
  "Baa" : "The sound a goat makes.",
  "Carpet": "Goes on the floor.",
  "Dab": "A small amount."
}

for key in webster:
  print webster[key]
```

### Controlling the flow of a loop
```
a = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

for n in a:
  if(n % 2 == 0):
    print n
  else:
    continue
```

### Count fizz
Make a function that will take in a list and count the number of times _fizz_ appears and return the count
```
def fizz_count(x):
  count = 0
  for item in x:
    if(item == 'fizz'):
      count += 1
    else:
      continue
  return count
```

### Make a store!
Now we're gonna make a little project!

```
prices = {
  'banana' : 4,
  'apple'  : 2,
  'orange' : 1.5,
  'pear'   : 3,
}

stock = {
  'banana' : 6,
  'apple'  : 0,
  'orange' : 32,
  'pear'   : 15,
}

# See inventory
for item in stock:
  print item
  print 'price: %s' % prices[item]
  print 'stock: %s' % stock[item]

# What would happen if we sold EVERYTHING in stock?
def pruge_stock():
  total = 0
  for item in prices:
    total += (prices[item] * stock[item])
  print total

# Given a shopping cart/grocery bag, compute the bill
def compute_bill(food):
  total = 0
  for item in food:
    if(stock[item] > 0):
     total += prices[item]
     stock[item] -= 1
  return total
```

## Quick lil challenge from codecademy
This block of code was just a challenge from the codecademy website...

Feel free to skip or review the code to see what is happening here!

```
lloyd = {
  "name": "Lloyd",
  "homework": [90.0, 97.0, 75.0, 92.0],
  "quizzes": [88.0, 40.0, 94.0],
  "tests": [75.0, 90.0]
}

alice = {
  "name": "Alice",
  "homework": [100.0, 92.0, 98.0, 100.0],
  "quizzes": [82.0, 83.0, 91.0],
  "tests": [89.0, 97.0]
}

tyler = {
  "name": "Tyler",
  "homework": [0.0, 87.0, 75.0, 22.0],
  "quizzes": [0.0, 75.0, 78.0],
  "tests": [100.0, 100.0]
}

# Add your function below!
def average(numbers):
  total = sum(numbers)
  total = float(total)
  return total / len(numbers)

def get_average(student):
  homework = average(student['homework'])
  quizzes = average(student['quizzes'])
  tests = average(student['tests'])
  
  return(.1 * homework) + (.3 * quizzes) + (.6 * tests)

def get_letter_grade(score):
  if(score >= 90):
    return 'A'
  elif(score >= 80):
    return 'B'
  elif(score >= 70):
    return 'C'
  elif(score >= 60):
    return 'D'
  else:
    return 'F'

def get_class_average(class_list):
  results = []
  for student in class_list:
    results.append(get_average(student))
  return average(results)

students = [alice, lloyd, tyler]

print get_class_average(students)
print get_letter_grade(get_class_average(students))
```

## Lists and Functions
Here we gooooooooooooooooooo!

### List Accessing
Peep how we pull information from a List my friend!
```
list = [1, 2, 3, 4]

print 'Second element of list is: %d' % list[1]
```

### List element modification
NOW PEEP how we modify some information in a list
```
list = [1, 2, 3, 4]

# multiply the second element by 10
list[1] *= 10

print list # <-- prints '[1, 20, 3, 4]' 
```

### Appending to a List
HINT HINT: use the `.append()` method to add elements to the end of a List
```
list = [1, 2, 3, 4]

# append an element to the end of a list
list.append(5)
```

See? It's easy!

### Removing elements from a List
There are many ways to remove an element from a list. Here, we will look at the three main ways to remove elements from a List!

1. `list.pop(index)` will remove the item at a provided index from the list and _return_ it to you!
2. `list.remove(item)` will remove the actual item if it finds it
3. `del(list[<some index>])` is similar to `.pop()` in that it will remove the item at the given index, but it *won't* return it!

Now lets look at some examples:
```
n = [1, 3, 5]

number = n.pop(0)
print 'number = %d' % number
print n

n.remove(5)
print n # <-- prints '[3]' 

del(n[0])  
print n # <-- prints '[]'
```

### Mulitple Parameter Functions
Lets look at a function that takes multiple arguments
```
m = 5
n = 13
def add_function(x, y):
  return x + y

print add_function(m, n)
```

### Strings in Functions
This is a recap on using strings in functions!
```
n = "Hello"

def string_function(s):
  return s + 'world'

print string_function(n)
```

### Passing a List into a Function
A list can be passed into a function just like any other argument!
```
def list_function(x):
  return x

n = [3, 5, 7]
print list_function(n)
```

### Using an element from a List in a Function
"Okay! Range time." <-- actual thing they put in this codecademy tutorial lol

The Python `range()` function is just a shortcut for generating a list, so you can use ranges in all the same places you can use lists.
```
def my_function(x):
  for i in range(0, len(x)):
    x[i] = x[i]
  return x

print my_function(range(3))
```

### Using strings in lists in functions
This is an easy one homie
```
n = ["Michael", "Lieberman"]
def join_strings(words):
  result = ""
  for word in words:
    result += word + " "
  return result

print join_strings(n)
```

### Using two lists as two args in a function
Concatinating lists in Python is actually stupid easy! Just use `+`
```
m = [1, 2, 3]
n = [4, 5, 6]

def join_lists(x, y):
  return x + y

print join_lists(m, n)
```

### Lists of lists too maybe? Hmmmm?
We can flatten(turning a collection of lists into a single list) lists in Python super easily too!
```
n = [[1, 2, 3], [4, 5, 6, 7, 8, 9]]

def flatten(lists):
  results = []
  for nums in lists:
    for num in nums: 
      results.append(num)
  return results

print flatten(n)
```

Is it possible to write a faster implementation? This is currently O(n^2)...

## BATTLESHIP
This is another mini project that can be skipped if you do not think it is necessary for you!

### Create the board
Here we are creating a 5x5 grid of Os (not zeros) to represent open ocean!
```
board = []

for i in range(0, 5):
  board.append(['O'] * 5)
```

And we can visualize the board with a function we will call `print_board()`. We could spam print statements to print every row of the board, but doing that manually is messy and *won't* scale for larger boards!
```
def print_board(board_in):
  for row in board_in:
    print ' '.join(row) # <-- this will remove the ugly quotation marks

print_board(board)
```

### Hide...
So now we want to place our ship in a random location! For this we'll need some random numbers! So we are going to use `from random import randint`. We want to account for both the row and the column since ships can horizontal or vertical, so we wil need two functions!
```
from random import randint
.
.
.
def random_row(board_in):
  return randint(0, len(board_in) -1)
def random_col(board_in):
  return randint(0, len(board_in) -1)
```

### ...and Seek!
Once we store the randomly generated coordinates, we have a hidden battleship in our ocean! Next we want to see if we can the hidden ships... this is BattleShip after all...
```
.
.
.
ship_row = random_row(board)
ship_col = random_col(board)

guess_row = int(raw_input('Guess Row: ')) # prompts user input
guess_col = int(raw_input('Guess Col: ')) # prompts user input
```

### Hit!
What if guess right?
```
if guess_row == ship_row and guess_col == ship_col:
  print 'Congratulations! You sank my battleship!'
```

### Error Checking
We have a couple cases to check for! Such as what if the guessed spot already has an *X*? What if the guess is out of range of the ocean? The following addresses these questions!
```
if guess_row == ship_row and guess_col == ship_col:
  print "Congratulations! You sunk my battleship!"
else:
  if (guess_row < 0 or guess_row > 4) or (guess_col < 0 or guess_col > 4):
    print "Oops, that's not even in the ocean."
  elif(board[guess_row][guess_col] == "X"):
    print "You guessed that one already."
  else:
    print "You missed my battleship!"
    board[guess_row][guess_col] = "X"
```

### Play it out!
All we need to do now is to make it an actual game that we can loop through to play! We just need to wrap it in a `for` or `while` loop! You can pick whichever, but here I use a for loop!
```
for turn in range(0, 4):
  guess_row = int(raw_input("Guess Row: "))
  guess_col = int(raw_input("Guess Col: "))

  if guess_row == ship_row and guess_col == ship_col:
    print "Congratulations! You sunk my battleship!"
    break
  else:
    if (guess_row < 0 or guess_row > 4) or (guess_col < 0 or guess_col > 4):
      print "Oops, that's not even in the ocean."
    elif(board[guess_row][guess_col] == "X"):
      print "You guessed that one already."
    else:
      print "You missed my battleship!"
      board[guess_row][guess_col] = "X"
    print turn + 1
    print_board(board)
  if turn == 3:
    print 'Game Over'
```

### Extra Credit
What if you want to add multiple ships? Make ships of different sizes? This can obviously get pretty in-depth, but for the sake of time, I'll either come back to this OR whoever is reading this can take the above code and submit a pull request to update the code block bellow!
```
Updated game code will go here when you're ready!
```
Have fun!

## Loops

We can make code run without calling things over and over again! When a piece of code is in a block that runs over and over for a certain amount of time, it is in a _loop_.

### While
A _while_ loop will run as long as the provided condition is true.
```
count = 0

if count < 5: # <-- will not print
  print "Hello, I am an if statement and count is", count

while count < 10: <-- will print the statement from 0 to 9
  print "Hello, I am a while and count is", count
  count += 1
```
We can catch errors in loops to! This is called _error handling_!
```
choice = raw_input('Enjoying the course? (y/n)')

while choice != 'n' and choice != 'y':
  choice = raw_input("Sorry, I didn't catch that. Enter again: ")
```

### Break
_What if want to exit a loop early, after a given condition is met?_

In these cases, we use the _break_ keyword. In general, we try to avoid using break, since sometime edge cases can cause _break_s to be skipped! This causes loops to produce unexpected output. 

*But* like anything else, using them sparingly is perfectly fine (_in my opinion..._)!
```
count = 0

while True:
  print count
  count += 1
  if count >= 10:
    break
```

### While/else
Python is cool bc it has this cool `while`/`else` construct. The `while` portion is like a normal `while` loop, but the `else` block will execute *anytime* the loop condition is evaluated to `False`. This means that the `else` block will execute if the loop is never entered, or if the loop exits normally.

If the `while` loop exits because of a `break`, then theb `else` block will not execute!
```
from random import randint

# Generates a number from 1 through 10 inclusive
random_number = randint(1, 10)

guesses_left = 3
# Start your game!
while guesses_left > 0:
  guess = int(raw_input('Please enter your number: '))
  if guess == random_number:
    print 'You win!'
    break
  guesses_left -= 1
else:
  print 'You lose.'
```

### For
An alternative way to loop... 

Here we use `i`(usually stands for _index_), and `range()` which takes up to three arguments!

`range(<start>, <stop>, <step>)`

For the most part, it's easiest to pass in a `<start>` and `<stop>`! If you only pass in one argument, the `range()` function will default to using the `<stop>` value and will start at 0.
```
print "Counting..."

for i in range(20):
  print i # <-- prints 0 to 19

## Next example below ##

hobbies = []

for i in range(3):
  hobby = str(raw_input('A hobbie?: '))
  hobbies.append(hobby)

print hobbies
```

### For your strings
Remember, strings are just *character arrays* in python!
```
thing = "spam!"

for c in thing:
  print c

print 'and'

word = "eggs!"

for c in word:
  print c
```

Another example but this time using a `,` in the print.

If you see something like `print x,` or `print "some string",` then that means keep the cursor on the same line!
```
phrase = "A bird in the hand..."

# Add your for loop
for char in phrase:
  if char == 'A' or char == 'a':
    print 'X' ,
  else:
    print char,

```

### For your lists
Loop through those list elements homie!
```
numbers  = [7, 9, 12, 54, 99]

print "This list contains: "

for num in numbers:
  print num

for num in numbers:
  print num * num
```

### Looping over dictionaries
You may be wondering how looping over a dictionary would work. Would you get the key or the value?

The short answer is: you get the key which you can use to get the value.

Lets look at a super simple example.
```
d = {'a': 'apple', 'b': 'berry', 'c': 'cherry'}

for key in d:
  print key, ' ', d[key]
```

### Counting as you go
Using the previous _for-each_ method of looping has it's downsides. One is not knowing the index of the thing you're looking at. This isn't an issue mostly, but at times it is useful to know how far into the list you are!

`enumerate` helps with this issue by supplying a corresponding index to each elelemt in the list that you pass it. Each time you go through the loop, `index` will be one greater, and `item` will be the next in the sequence.

This is simliar to a normal `for` loop, but this gives us an easy way to count how many items we've seen so far.
```
choices = ['pizza', 'pasta', 'salad', 'nachos']

print 'Your choices are:'
for index, item in enumerate(choices):
  print index + 1, item
```

### Multiple Lists
It's common to need to iterate over two lists at once. This is where `zip` comes in.

`zip` creates pairs of elements when two lists are passed in, and stops at the end of the *shorter* list!

`zip` can handle three or more lists!
```
list_a = [3, 9, 17, 15, 19]
list_b = [2, 4, 8, 10, 30, 40, 50, 60, 70, 80, 90]

for a, b in zip(list_a, list_b):
  if a > b:
    print a
  else:
    print b
```

### For/else
Well well well... look at this... another /`else`!

In the case of a `for`/`else`, the `else` statement is executed after the `for`, but only if the `for` ends normally - that is, not with a `break`.
```
fruits = ['banana', 'apple', 'orange', 'tomato', 'pear', 'grape']

print 'You have...'
for f in fruits:
  if f == 'tomato':
    print 'A tomato is not a fruit!' # (It actually is.) 
    break
  print 'A', f
else:
  print 'A fine selection of fruits!'
```
Let's see what happens when we remove the tomato...
```
fruits = ['banana', 'apple', 'orange', 'pear', 'grape']

print 'You have...'
for f in fruits:
  if f == 'tomato':
    print 'A tomato is not a fruit!' # (It actually is.)
    break
  print 'A', f
else:
  print 'A fine selection of fruits!'
```
What do you see when you run the above?

## Prcatice, practice, practice
Lil practice section! You can skip if you want!

### Interesting little function to check if input is an Integer
```
"""
This function works for inputs such as <7.0, 7, -7>
"""

def is_int(x):
  absolute = abs(x)
  rounded = round(absolute)
  return absolute - rounded == 0 
```

### Factorial
```
def factorial(x):
  total = 1
  for i in range(1, x + 1):
    total = total * i
  return total 
```

### Reverse a string
```
def reverse(text):
  new_text = ''
  letter = len(text) - 1
  while letter >= 0:
    new_text += text[letter]
    letter -= 1
  return new_text

print reverse('abcd')
```

### Remove vowels from a string
```
def anti_vowel(text):
  new_text = ''
  vowels = ['a','e','i','o','u']
  for i in range(0, len(text)):
    if not text[i].lower() in vowels:
      new_text += text[i]
  return new_text
```

### Scrabble Score (Dictionary practice)
```
score = {"a": 1, "c": 3, "b": 3, "e": 1, "d": 2, "g": 2, 
         "f": 4, "i": 1, "h": 4, "k": 5, "j": 8, "m": 3, 
         "l": 1, "o": 1, "n": 1, "q": 10, "p": 3, "s": 1, 
         "r": 1, "u": 1, "t": 1, "w": 4, "v": 4, "y": 4, 
         "x": 8, "z": 10}

def scrabble_score(word):
  word = word.lower()
  total = 0
  """
  In the following for loop you have two options. 
  
  You can access the score dictionary using the standard accessing method: 
	score[key]

  OR 

  You can access the score dictionary using .get(). 
	This method allows us to return a default value in case the key is not present in the dictionary.  
  """
  for c in word:
    total += score.get(c, 'LETTER N0T FOUND!')
  return total
```

### Easy string replacement in sentecne
```
def censor(text, word):
  return text.replace(word, '*' * len(word))
```

### Count occurences in a list
```
def count(sequence, item):
  total = 0
  for i in sequence:
    if i == item:
      total += 1
  return total
```

### Remove odd numbers from a list
```
def purify(list_):
  new_list = []
  for element in list_:
    if element % 2 == 0:
      new_list.append(element)
  return new_list
```

### Get product of all numbers in a list
```
def product(list_):
  total = 1
  for element in list_:
    total *= element
  return total
```

### Some tricks to remove duplicates from a list
```
test_list = ['a', 'a', 'a', 'b', 'b', 'c', 'c', 'c', 'd']
"""
# Write custome function
def remove_duplicates(list_):
  res = []
  for element in list_:
    if element not in res:
      res.append(element)
  return res

# Use dictionary to remove dupes
def remove_duplicates(list_):
  return list(OrderedDict.fromkeys(list_))

# Use set to remove dupes. NO ORDER 
def remove_duplicates(list_):
  return list(set(list_))
"""

print remove_duplicates(test_list)
```

### Find median value in list
```
def median(lst):
    sorted_list = sorted(lst)
    if len(sorted_list) % 2 != 0:
        index = len(sorted_list)//2 
        return sorted_list[index]
    elif len(sorted_list) % 2 == 0:
        index_1 = len(sorted_list)/2 - 1
        index_2 = len(sorted_list)/2
        mean = (sorted_list[index_1] + sorted_list[index_2])/2.0
        return mean
   
print median([2, 4, 5, 9])
```

## Grade Calculator
This is another example program and can be skipped if you don't need a refresher on anything!

### Main Program
This program accepts a list of grades, and will do various calculations on them!
```
grades = [100, 100, 90, 40, 80, 100, 85, 70, 90, 65, 90, 85, 50.5]

def print_grades(grades_input):
  print 'Loading all grades...'  
  for grade in grades_input:
    print grade

def grades_sum(scores):
  total = 0
  for score in scores: 
    total += score
  return total

def grades_average(grades_input):
  sum_of_grades = grades_sum(grades_input)
  average = sum_of_grades / float(len(grades_input))
  return average

def grades_variance(scores):
  average = grades_average(scores)
  variance = 0
  for score in scores:
    variance += (average - score) ** 2
  return variance / len(scores)

def grades_std_deviation(variance):
  return variance ** 0.5

variance = grades_variance(grades)
total = grades_sum(grades)
avg = grades_average(grades)
var = grades_variance(grades)
std_dev = grades_std_deviation(var)

print_grades(grades)
print 'Sum of grades: ', total
print 'Average of grades: ', avg
print 'Variance of grades: ', var
print 'Standard deviation of grades: ', std_dev
```

## Advanced Topics
Let's look at some more advanced python-y things, shall we?

### Iterators for Dictionaries
We can use the `.items()` Dictionary method to get the key/value pairs from a dictionary.

*NOTE:* This is usually *will not* return the key/value pairs in order!
```
my_dict = {
  'Name' : 'Lake',
  'Age' : 15,
  'Dev' : True,
  'IT Guy' : False,
  'Favorite Foods' : [
    'Ramen',
    'Udon',
    'Pho',
    'Soba'
  ],
}

print my_dict.items()
```

### keys() and values()
The `.keys()` method returns a _list_ of the dictionary's keys.

The `.values()` method returns a _list_ of the dictionary's values.
```
my_dict = {
  'Name' : 'Lake',
  'Age' : 15,
  'Dev' : True,
  'IT Guy' : False,
  'Favorite Foods' : [
    'Ramen',
    'Udon',
    'Pho',
    'Soba'
  ],
}

print my_dict.keys()
print my_dict.values()
```

### Using 'in'
`in` is Python's keyword used for lists, tuples, dictionaries, and strings!

You could say it's very, `in`_tuitive_...
```
my_dict = {
  'Name' : 'Lake',
  'Age' : 15,
  'Dev' : True,
  'IT Guy' : False,
  'Favorite Foods' : [
    'Ramen',
    'Udon',
    'Pho',
    'Soba'
  ],
}

for key in my_dict:
  print key, ':', my_dict[key]
```
Here we see, that `key` is the key in the dictionary associated with the value at that key! We can access this value intuitively by using array/list syntax; `my_dict[key]`.

### Building Lists
We can easily build lists in Python with something like, `lst = range(51)`; which builds a list of numbers from 0 to 50 inclusive.

But what if wanted our list to contain numbers pertaining to certain logic - i.e. numbers containing even numbers.

In Python, we can use a _list comprehension_! This allows us to generate a list following our own defined logic in one line!
```
evens_to_50 = [i for i in range(51) if i % 2 == 0]

print evens_to_50
```

### List Comprehension Syntax
Do a lil practice!
```
doubles_by_3 = [x * 2 for x in range(1, 6) if (x * 2) % 3 == 0]

even_squares = [i ** 2 for i in range(1, 11) if (i ** 2) % 2 == 0]

cubes_by_four = [x ** 3 for x in range(1, 11) if (x ** 3) % 4 == 0]

print doubles_by_three
print even_squares
print cubes_by_four
```

### Slice it up
Sometimes we only want part of list! We can _slice_ a list to get only the portion we want!

We use a colon, `:`, to _slice_ a list!
```
l = [i ** 2 for i in range(1, 11)]

print l[2:9:2]
```

### Omitting Indices
If you don't pass a particular index to the list slice, Python will pick a default.

We can specify a _stride_ to skip certain elements in a list! To show this, we will print a list's odd elements by specifying a stride of every other element. 
```
my_list = range(1, 11) # List of numbers 1 - 10

print my_list[::2] # Stride
```

### Reversing a list
We can easily reverse a list by using a _reverse stride_!
```
my_list = range(1, 11)

backwards = my_list[::-1]

print backwards
```

### Stride Length
A positive stride length traverses the list from left to right, and a negative one traverses the list from right to left.

Further, a stride length of 1 traverses the list “by ones,” a stride length of 2 traverses the list “by twos,” and so on.
```
to_one_hundred = range(101)

backwards_by_tens = to_one_hundred[::-10]

print backwards_by_tens
```

### Mini Practice
```
o_21 = range(1, 22) # <-- create list from 1 - 21

odds = [x for x in to_21 if (x % 2) == 1] # <-- create list of odd numbers within the the range of our to_21 list 

middle_third = to_21[7:14] # <-- get the middle third (slice of 8 to 14) from the to_21 list 
```

### Anonymous Functions
Well, well, well we meet again _functional programming_. It's been a long time...

_Functional programming_ allows us to pass functions arround as if they were variables.

To start a new anonymous function, we use the `lambda` key word? Why? Look up the _Haskell Programming Language_ to find out!

Lambda functions are defined using the following syntax.
```
my_list = range(16)
filter(lambda x: x % 3 == 0, my_list)
```

Lambdas are useful for when you need a function to do some quick work! If you plan on creating a function you’ll use over and over, you’re better off using def and giving that function a name. 

Let's see some simple examples!
```
"""
    Using filter() to simply filter a list!
"""
languages = ["HTML", "JavaScript", "Python", "Ruby"]

print filter(lambda x: x == "Python", languages)

"""
    Fill a list with the squared values of 1 to 10, and then print if you are in a certain range!
"""
squares = [x ** 2 for x in range(1, 11)]

print filter(lambda x: x in range(30, 71), squares)
```

## Bitwise Operations
Bitwise operations can sound a little tricky at first, but once you get them, they become a powerful tool at your disposal! Most languages have them, from Java, JavaScript, C/C++, Go, and Ruby! 

_Okay cool, but what exactly are Bitwise Operations?_

_Bitwise operations_ are operations that allow the programmer to directly manipulate bits. In all computers, almost everthing is represented by bits, a series of zeros and ones. 

Now you won't often see bitwise operations in everyday programming, but when you do see them, you should know what is going on!

### Lesson 10: The Base 2 Number System
Here's a little numbers lesson for ya!

When we count, we do it in base 10. That means that each place in a number can hold one of ten values: 0-9. 

In _binary_, we count in base two, where each place can hold one of two values: 0 or 1. The counting pattern is the same as in base 10 except when you carry over to a new column, you have to carry over every time a place goes higher than one (as opposed to higher than 9 in base 10). 

For example, the numbers one and zero are the same in base 10 and base 2. But in base 2, once you get to the number 2 you have to carry over the one, resulting in the representation “10”. Adding one again results in “11” (3) and adding one again results in “100” (4).

Contrary to counting in base 10, where each decimal place represents a power of 10, each place in a binary number represents a power of two (or a bit). The rightmost bit is the 1’s bit (two to the zero power), the next bit is the 2’s bit (two to the first), then 4, 8, 16, 32, and so on.

In Python, you an write numbers in binary using appending `0b` to a sequence of zeros and ones.
```
print 0b1,    # 1
print 0b10,   # 2
print 0b11,   # 3
print 0b100,  # 4
print 0b101,  # 5
print 0b110,  # 6
print 0b111   # 7
print "******"
print 0b1 + 0b11 # 4
print 0b11 * 0b11 # 9
```

### The bin() function
There are functions that can help you understand and write bitwise operations!

The `bin()` function accepts a single integer as an argument, and returns the binary representation of that number in a string!

Remember, `bin()` takes a number, and returns a string!

### int()'s second parameter
What you might not know is that the `int()` function actually has an optional second parameter.

When given a string containing a number and the base that number is in, the function will return the value of that number converted to base ten.
```
print int("1",2) # 1
print int("10",2) # 2
print int("111",2) # 7 
print int("0b100",2) # 4
print int(bin(5),2) # 5
print int("0b11001001", 2) # 201
```

### Shifting left and right
The next two operations we are going to talk about are the left and right shift bitwise operators. These operators work by shifting the bits of a number over by a designated number of slots.

Shift operations are similar to rounding down after dividing and multiplying by 2 (respectively) for every time you shift, but it’s often easier just to think of it as shifting all the 1s and 0s left or right by the specified number of slots.

*Note that you can only do bitwise operations on an integer. Trying to do them on strings or floats will result in nonsensical output!*
```
shift_right = 0b1100 
shift_left = 0b1

print 'Before: '
print bin(shift_right)
print bin(shift_left)

print '\n'

shift_right = shift_right >> 2
shift_left = shift_left << 2

print 'After'
print bin(shift_right)
print bin(shift_left)
```

### A bit of this AND that
The bitwise AND `&` operator compares two numbers on a bit level and returns a number where the bits of that number are turned on if the corresponding bits of *both* numbers are 1.
```
print bin(0b1110 & 0b101) # <-- 0b100
```

### A bit of this OR that
The bitwise OR `|` operator compares two numbers on a bit level and returns a number where the bits of that number are turned on if *either* of the corresponding bits of either number are 1.
```
print bin(0b1110 | 0b101) # <-- 0b1111
```

### This XOR that?
The XOR `^` or exclusive or operator compares two numbers on a bit level and returns a number where the bits of that number are turned on if *either* of the corresponding bits of the two numbers are 1, *but not both*.
```
print bin(0b1110 ^ 0b101) # <-- 0b1011
```

### NOT so hard, is it? 
The bitwise NOT operator `~` just flips all of the bits in a single number. What this actually means to the computer is actually very complicated, so we’re not going to get into it. Just know that mathematically, this is equivalent to adding one to the number and then making it negative.

### A bit mask
A _bit mask_ is just a variable that aids you with bitwise operations. A bit mask can help you turn specific bits on, turn others off, or just collect data from an integer about which bits are on or off.
```
def check_bit4(num):
  mask = 0b1000
  if mask & num > 0:
    return "on"
  else:
    return "off"
```

### Turn it on
You can also use masks to turn a bit in a number on using `|`.

For example lets turn the third bit to on for number represented in binary
```
a = 0b10111011
mask = 0b0100
desired = a | mask
print bin(desired) # <-- 0b10111111
```

### Flip it homie
Using the XOR `^` operator is very useful for flipping bits. Using `^` on a bit with the number one will return a result where that bit is flipped!
```
a = 0b11101110
mask = 0b11111111
desired = a ^ mask
print bin(desired) # <-- 0b10001
```

### Slide it homie
Finally, you can also use the left shift `<<` and right shift `>>` operators to slide masks into place.
```
def flip_bit(number, n):
  mask = (0b1 << (n-1)) # <-- We use (n-1) because we only need to slide the mask over n number of places from the FIRST bit to reach our true n!
  result = mask ^ number
  return bin(result)
```

## Classes
Python is an _object-oriented_ programming language, which means it manipulates programming constructs called _objects_.

Think of an object as a single that contains data as well as functions; the functions of an object are called its _methods_! For example...

When you call `len('Eric')`, Python is checking to see whether the string object you passed has a length, and if it does, it rutens the value associated with that attribute.

When you call `some_dict.itmes()`, Python checks to see if `some_dict` has an `items()` method (which ALL dictionaries have) and executes that method if it finds it!

### Class Syntax
A basic class only consists of the the `class` keyword, the name of the class, and the class from which the new class *inherits* in parentheses. (Which will be covered later so for now we will inherit from the `object` class!)
```
class Animal(object):
  pass
```

What does the `pass` keyword do? Exactly what it looks like! Here it is a placeholder for our object body until we further implement!

### Classier Classes
Lets say we want our classes to do more than... _nothing_ aka `pass`...

We need to *initialize* the objects our `Animal` object creates! We will use the standard `__init__()` function. The `__init__()` function will *ALWAYS* take at least one argument: `self`. `self` refers to the object being created.
```
class Animal(object):
  def __init__(self):
    pass
```

### Not too selfish
`self` is a Python convention; there’s nothing magic about the word `self`. However, it’s overwhelmingly common to use `self` as the first parameter in `__init__()`, so you should do this so that other people will understand your code.

The part that is magic is the fact that `self` is the first parameter passed to `__init__()`. Python will use the first parameter that `__init__()` receives to refer to the object being created; this is why it’s often called `self`, since this parameter gives the object being created its identity. This becomes a lot more clear in the next few example code snips!
```
class Animal(object):
  def __init__(self, name):
    self.name = name

zebra = Animal("Jeffrey")

print zebra.name
```

### A bigger self example
```
class Animal(object):
  """Makes cute animals."""
  # For initializing our instance objects
  def __init__(self, name, age, is_hungry):
    self.name = name
    self.age = age
    self.is_hungry = is_hungry

# Note that self is only used in the __init__()
# function definition; we don't need to pass it
# to our instance objects.

zebra = Animal("Jeffrey", 2, True)
giraffe = Animal("Bruce", 1, False)
panda = Animal("Chad", 7, True)

print zebra.name, zebra.age, zebra.is_hungry
print giraffe.name, giraffe.age, giraffe.is_hungry
print panda.name, panda.age, panda.is_hungry
```

### Class Scope
Another important aspect of Python classes is _scope_. The _scope_ of a variable is the context in which it’s visible to the program.

It may surprise you to learn that not all variables are accessible to all parts of a Python program at all times. When dealing with classes, you can have variables that are available everywhere (_global variables_), variables that are only available to members of a certain class (_member variables_), and variables that are only available to particular instances of a class (_instance variables_).

The same goes for functions: some are available everywhere, some are only available to members of a certain class, and still others are only available to particular instance objects.

In the below example, we can see that `is_true` is a public member variable in the Animal class!
```
class Animal(object):
  """Makes cute animals."""
  is_alive = True
  def __init__(self, name, age):
    self.name = name
    self.age = age

zebra = Animal("Jeffrey", 2)
giraffe = Animal("Bruce", 1)
panda = Animal("Chad", 7)

print zebra.name, zebra.age, zebra.is_alive
print giraffe.name, giraffe.age, giraffe.is_alive
print panda.name, panda.age, panda.is_alive
```

### Adding your own methods!
Below is an example of a description method that will print out some of the `Animal` object attributes!
```
class Animal(object):
  """Makes cute animals."""
  is_alive = True
  def __init__(self, name, age):
    self.name = name
    self.age = age
  # Add your method here!
  def description(self):
    print self.name
    print self.age
  
hippo = Animal("Jessica", 27)
```

### Member variable access
A class can have any number of *member variables*. By default, a class' member variables are public by default!

We can use certain conventions if we want to make a member variable private, but for now, don't worry about it!
```
class Animal(object):
  """Makes cute animals."""
  is_alive = True
  health = "good"
  def __init__(self, name, age):
    self.name = name
    self.age = age
  # Add your method here!
  def description(self):
    print self.name
    print self.age
  
hippo = Animal("Jessica", 27)
sloth = Animal("Andrew", 25)
ocelot = Animal("Corey", 26)

print hippo.health
print sloth.health
print ocelot.health
```

### [WARNING]: Here Be Dragons
_Inheritance_ is a tricky concept, so let’s go through it step by step.

Inheritance is the process by which one class takes on the attributes and methods of another, and it’s used to express an is-a relationship. For example, a Panda is a bear, so a Panda class could inherit from a Bear class. However, a Toyota is not a Tractor, so it shouldn’t inherit from the Tractor class (even if they have a lot of attributes and methods in common). Instead, both Toyota and Tractor could ultimately inherit from the same Vehicle class.

### Syntax
In general, Python inheritance looks like this:
```
class DerivedClass(BaseClass):
  # code goes here
```
Where `DerivedClass` is the new class, and `BaseClass` is the class from which the new class inherits from.

Here is a more concrete example:
```
class Shape(object):
  """Makes shapes!"""
  def __init__(self, number_of_sides):
    self.number_of_sides = number_of_sides

class Triangle(Shape):
  """Make shapes!"""
  def __init__(self, side1, side2, side3):
    self.side1 = side1
    self.side2 = side2
    self.side3 = side3
```

### Override!
Sometimes you’ll want one class that inherits from another to not only take on the methods and attributes of its parent, but to override one or more of them.

Overriding allows us to have a method implementation separate from our base class method!
```
class Employee(object):
  """Models real-life employees!"""
  def __init__(self, employee_name):
    self.employee_name = employee_name

  def calculate_wage(self, hours):
    self.hours = hours
    return hours * 20.00

class PartTimeEmployee(Employee):
  def calculate_wage(self, hours):
    self.hours = hours
    return hours * 12.00
```
As you can see from the example above, we have a separate implementation for the `calculate_wage` method, but it has the same name as the one in the `Employee` class.

### Subz
On the flip side, sometimes you’ll be working with a derived class (or _subclass_) and realize that you’ve overwritten a method or attribute defined in that class’ base class (also called a parent or superclass) that you actually need. Have no fear! You can directly access the attributes or methods of a superclass with Python’s built-in `super` call.

The syntax looks like this:
```
class Derived(Base):
  def m(self):
    return super(Derived, self).m()
```
Where `m()` is the method from the base class!

```
class Employee(object):
  """Models real-life employees!"""
  def __init__(self, employee_name):
    self.employee_name = employee_name

  def calculate_wage(self, hours):
    self.hours = hours
    return hours * 20.00

class PartTimeEmployee(Employee):
  def calculate_wage(self, hours):
    self.hours = hours
    return hours * 12.00
  
  def full_time_wage(self, hours):
    return super(PartTimeEmployee, self).calculate_wage(hours)


milton = PartTimeEmployee('Milton')

print milton.full_time_wage(10) # <-- returns 200.0

# ANOTHA ONE

class Triangle(object):
  
  """
    memeber variables
  """
  number_of_sides = 3
  
  """
    constructor
  """
  def __init__(self, angle1, angle2, angle3):
    self.angle1 = angle1
    self.angle2 = angle2
    self.angle3 = angle3
  
  """
    member functions
  """
  def check_angles(self):
    return True if (self.angle1 + self.angle2 + self.angle3 == 180) else False 

class Equilateral(Triangle):
  angle = 60
  
  def __init__(self):
    self.angle1 = self.angle
    self.angle2 = self.angle
    self.angle3 = self.angle
    
my_triangle = Triangle(90, 30, 60)

print my_triangle.number_of_sides
print my_triangle.check_angles()
```

## File I/O
Until now, the Python code in this document has been single source code. It does one thing, and returns something to the terminal/cli. But wwhat if you want to read/write some data from a file on your computer?

This process is called _I/O_ (_input/output_), and Python has some built-ins to handle this for you!

### open()
The `open()` function is self explanitory, but it has many uses so I'll go through some examples...

The below example takes a text file called `output.txt` and a permissions argument. Here, we will use `'r+'` which allows us to read and write to the file. 
```
my_file = open('output.txt', 'r+')
```

Now, what if we want to write some data to a new text file? This time, we will use `'w'` which will give us write permissions to the file we want to write data to. The first argument, `output.txt`, is the name of the file we want to create in the current working directory. It is always good practice to use the `.close()` function when you are done with a file too!
```
my_list = [i ** 2 for i in range(1, 11)]

my_file = open("output.txt", "w")

for i in my_list:
  my_file.write(str(i) + '\n')

my_file.close()
```

We want to be able to read data too, right? For this, we will use the `.read()` function! If we just want to read the file, we will use the `'r'` permission, which will ensure that we are just going to open the file, read it, and close it. This way we can avoid unintended behavior from the function!
```
my_file = open('output.txt', 'r')

print my_file.read()

my_file.close()
```

What if we want to read a file line by line, instead of the whole thing at once(_For control flow_)? We will use, `.readline()` for this! Below, you will see that since we are not running this in a loop, then we will only see the first three lines from the file!
```
my_file = open('text.txt', 'r')

print my_file.readline()
print my_file.readline()
print my_file.readline()

my_file.close()
```

### PSA: Buffering Data
We keep telling you that you always need to close your files after you’re done writing to them. Here’s why!

During the I/O process, data is buffered: this means that it is held in a temporary location before being written to the file.

Python doesn’t flush the buffer—that is, write data to the file—until it’s sure you’re done writing. One way to do this is to close the file. If you write to a file without closing, the data won’t make it to the target file.
```
# Use a file handler to open a file for writing
write_file = open("text.txt", "w")

# Open the file for reading
read_file = open("text.txt", "r")

# Write to the file
write_file.write("Not closing files is VERY BAD.")
write_file.close()

# Try to read from the file
print read_file.read()
read_file.close()
```

### `with` and `as`
Programming is all about getting the computer to do the work. Is there a way to get Python to automatically close our files for us?

*Of course there is. This is Python.*
You may not know this, but file objects contain a special pair of built-in methods: `__enter__()` and `__exit__()`. The details aren’t important, but what is important is that when a file object’s `__exit__()` method is invoked, it automatically closes the file. How do we invoke this method? With `with` and `as`.
```
with open('text.txt', 'w') as my_file:
  my_file.write('hello?')
```

### Case closed?
Finally, we’ll want a way to test whether a file we’ve opened is closed. Sometimes we’ll have a lot of file objects open, and if we’re not careful, they won’t all be closed. How can we test this?

Python file objects have a `closed` attribute which is `True` when the file is closed and `False` otherwise.

By checking `file_object.closed`, we’ll know whether our file is closed and can call `close()` on it if it’s still open.
```
with open('text.txt', 'w') as my_file:
  my_file.write('hello?')

if my_file.closed == False:
  my_file.close()

print my_file.closed
```